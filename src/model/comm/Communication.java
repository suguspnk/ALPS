package model.comm;

import java.sql.ResultSet;
import java.sql.SQLException;

import view.comm.Comm_Tab;
import model.SQLConnection;
import controller.Comm_Controller;
import controller.Home_Controller;

public class Communication {


	public static final String tableName = "communication";
	public static int numCommunication;
	String title, sender, path, date, user_id;
	private int id, type;
	
	public Communication(){
		
	}
	
	public Communication(int id, int type, String sender, String date, String path, String title, String user_id){
		setId(id);
		setValues(type, sender, date, path, title, user_id);
	}
	
	public void updateCommunication(int id, String date, String title, int type, String sender, String path) throws Exception{
		path = path.replace("\\", "/");
		SQLConnection.getInstance().update("update "+tableName+" set type = "+type+", to_or_from = '"+SQLConnection.insertBackSlash(sender)+"', date = '"+date+"', filename = '"+SQLConnection.insertBackSlash(path)+"', "
											+ "title = '"+SQLConnection.insertBackSlash(title)+"' where id = "+id);
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public int getType() {
		return type;
	}
	
	public String getSender() {
		return sender;
	}
	
	public String getPath() {
		return path;
	}
	
	public String getDate() {
		return date;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	public void setSender(String sender) {
		this.sender = sender;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public void setDate(String date) {
		this.date = date;
	}

	public void add(int type, String sender, String date, String path, String title, String user_id) throws Exception {
/*		System.out.println("insert into "+tableName+"(type, to_or_from, date, filename, title, user_id) values ("+type+", '"+SQLConnection.insertBackSlash(sender)+"', '"+date+"', '"+SQLConnection.insertBackSlash(path)+"', "
				+ "'"+SQLConnection.insertBackSlash(title)+"', "+user_id+")");*/
		setValues(type, sender, date, path, title, user_id);
		path = path.replace("\\", "/");
		Comm_Controller.sqlConnect.update("insert into "+tableName+"(type, to_or_from, date, filename, title, user_id) values ("+type+", '"+SQLConnection.insertBackSlash(sender)+"', '"+date+"', '"+SQLConnection.insertBackSlash(path)+"', "
				+ "'"+SQLConnection.insertBackSlash(title)+"', "+user_id+")");
	}
	
	private void setValues(int type, String sender, String date, String path, String title, String user_id) {
		setType(type);
		setSender(sender);
		setDate(date);
		setPath(path);
		setTitle(title);
		setUser_id(user_id);
	}

	public static boolean exists(String username) throws ClassNotFoundException, SQLException {
		ResultSet rs = Comm_Controller.sqlConnect.query("select id from "+tableName+" where username = '"+SQLConnection.insertBackSlash(username)+"'");
		try{
			if(rs.next())
				return true;
		}catch(Exception e){}
		return false;
	}

	public static boolean hasUsers() throws ClassNotFoundException, SQLException {
		ResultSet rs = Comm_Controller.sqlConnect.query("select id from user");
		try{
			if(rs.next())
				return true;
		}catch(Exception e){}
		return false;
	}
	
	public static int getNumCommunication(String query) throws ClassNotFoundException, SQLException{
		ResultSet numRs = Home_Controller.sqlConnect.query(query);
		while(numRs.next())
			return numRs.getInt(1);
		return 0;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public static void setNumCommunication(int numCommunication) {
		Communication.numCommunication = numCommunication;
		Comm_Tab.numPages = (numCommunication-1)/15+1;
	}
}