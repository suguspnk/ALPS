package model.comm;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.SQLConnection;

public class Comm_List {
	SQLConnection sqlConnect;
	
	ArrayList<Communication> list = new ArrayList<>();
	String columnName[] = {"title", "type", "to_or_from", "filename"};
	
	public Comm_List() {
		sqlConnect = SQLConnection.getInstance();
	}
	
	public void updateList(int selectedIndex, String searchString, int offset) throws ClassNotFoundException, SQLException{
		list.clear();
		System.gc();
		ResultSet rs = null;
		final byte LIMIT = 15;
		String query = "";
		if (selectedIndex==0)
			query = "select * from "+Communication.tableName;
		else {
			String search = selectedIndex!=2 ? "LIKE '%"+SQLConnection.insertBackSlash(searchString)+"%'" : (searchString.equalsIgnoreCase("incoming") ? "= 0" : (searchString.equalsIgnoreCase("outgoing") ? "= 1" : "= -1"));
			query = "select * from "+Communication.tableName+" where "+columnName[(selectedIndex-1)]+" "+search;
		}
		rs = sqlConnect.query(query+" order by id desc limit "+offset+", "+LIMIT);
		while(rs.next())
			list.add(new Communication(rs.getInt("id"), rs.getInt("type"), rs.getString("to_or_from"), rs.getString("date"), rs.getString("filename"), rs.getString("title"), ""+rs.getInt("user_id")));
		Communication.setNumCommunication(Communication.getNumCommunication(query.replace("select *", "select count(id)")));
	}
	
	public int size() {
		return list.size();
	}

	public Communication get(int i) {
		return list.get(i);
	}
}
