package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLConnection {

	public SQLConnection(){
		
	}
	
	private static Connection createConnection(String database, String hostname, String username, String password, String port) throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
        Connection connection=DriverManager.getConnection("jdbc:mysql://"+hostname+":"+port+"/"+database, Config.user, password);
        return connection;
	}
	
	private static Connection createMySQLConnection(String hostname, String password, String port) throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
        Connection connection=DriverManager.getConnection("jdbc:mysql://"+hostname+":"+port, Config.user, password);
        return connection;
	}
	
	public void createDB() throws IOException, ClassNotFoundException, SQLException{
		Connection conn = createMySQLConnection(Config.hostname, Config.password, Config.port);
		Statement stmt = conn.createStatement();
		BufferedReader reader = new BufferedReader(new FileReader(new File("db/database.sql")));
		String line = "", fullLine = "";
		while((line = reader.readLine()) != null){
			if(line.startsWith("-- "))
				continue;
			fullLine+=line;
			if(fullLine.indexOf(";") == -1)
				continue;
			stmt.executeUpdate(fullLine);
			fullLine = "";
		}
		reader.close();
	}
	
	public void update(String update) throws Exception{
		closeConnection();
		try{
			conn = createConnection(Config.database, Config.hostname, Config.user, Config.password, Config.port);
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(update);
			conn.commit();
			conn.close();
		}catch(SQLException | ClassNotFoundException e){
			conn.rollback();
			conn.close();
			throw e;
		}
	}
	
	Connection conn ;
	public ResultSet query(String query) throws ClassNotFoundException, SQLException{
		closeConnection();
		conn = createConnection(Config.database, Config.hostname, Config.user, Config.password, Config.port);
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		return rs;
	}
	
	//used if a connection is still open and another query must be executed
	public ResultSet queryTwo(String query) throws ClassNotFoundException, SQLException{
		conn = createConnection(Config.database, Config.hostname, Config.user, Config.password, Config.port);
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		return rs;
	}
	
	public void closeConnection() throws SQLException {
		if(conn != null && !conn.isClosed()){
			conn.close();
			conn = null;
		}
	}
	
	private static SQLConnection instance;
	
	public static SQLConnection getInstance(){
		if(instance == null)
			instance = new SQLConnection();
		return instance;
	}

	public static void checkConnection(String password) throws ClassNotFoundException, SQLException {
		Connection conn = createMySQLConnection(Config.hostname, password, Config.port);
		Config.password = password;
		conn.close();
	}
	
	/**
	 * this method is used to avoid SQL injection. this method will insert a backslash if a character is not a letter or a number
	 * @param str
	 * @return
	 */
	public static String insertBackSlash(String str) {
		String result = "";
		for(int i = 0; i < str.length(); result+=str.charAt(i++))
			if(str.charAt(i) == '\'' || str.charAt(i) == '\"')	// if the character is not a letter or a number
				result+="\\";
		return result;
	}
}