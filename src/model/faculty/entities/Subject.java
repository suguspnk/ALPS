package model.faculty.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import controller.Home_Controller;
import model.SQLConnection;

public class Subject extends Entity {

	public static ArrayList<Subject> list;
	public String title;
	public String time;
	public String room;
	public String syllabusFile;
	public int sem;
	public int id;
	public int facultyId;
	
	public String prompt;
	
	public Subject(int id, String subject, String time, String room, String filepath, int sem, int facultyId){
		this.id = id;
		this.facultyId = facultyId;
		this.room = room;
		this.facultyId = facultyId;
		this.title = subject;
		this.time = time;
		this.syllabusFile = filepath;
		this.sem = sem;
	}
	
	public boolean addOrUpdate(int choice){
		
		prompt = "";
		
		if(choice == 0){
			try{
				String insert_subject = "INSERT INTO `subject` (subject, time, room, semester, filename, faculty_id) VALUES"
						+"('" + SQLConnection.insertBackSlash(title) + "','" + SQLConnection.insertBackSlash(time) + "','"
						+ SQLConnection.insertBackSlash(room) + "','" + sem + "','" + SQLConnection.insertBackSlash(syllabusFile)+ "','"
						+ facultyId + "');\n";
				System.out.println(insert_subject);
				Home_Controller.sqlConnect.update(insert_subject);
			} catch (Exception e){
				e.printStackTrace();
				prompt = "There is an error adding a subject!";
				return false;
			}
			prompt = "Successfully added a new subject!";
			return true;
		}
		else if(choice == 1){
			String update_subject = "UPDATE subject SET subject = '" + SQLConnection.insertBackSlash(title) + "', time = '" 
					+ SQLConnection.insertBackSlash(time) + "', room = '" + SQLConnection.insertBackSlash(room) + "', semester = "
					+ sem + ", filename = '" + SQLConnection.insertBackSlash(syllabusFile) + 
					"' WHERE id = " + id + ";";
			try{
				Home_Controller.sqlConnect.update(update_subject);									
			} catch (Exception e){
				e.printStackTrace();
				prompt = "There was an error encountered while updating the subject!";
				return false;
			}
			prompt = "Successfully edited the subject!";
			return true;
		}
		return false;
	}
	
	public static ArrayList<Subject> getSubjects(int facultyId) throws ClassNotFoundException, SQLException {
		String query = "select * from subject where faculty_id = "+facultyId+" order by subject asc";
		ResultSet rs = Home_Controller.sqlConnect.query(query);
		ArrayList<Subject> list = new ArrayList<>();
		while(rs.next())
			list.add(new Subject(rs.getInt("id"), rs.getString("subject"), rs.getString("time"), rs.getString("room"), rs.getString("filename"), rs.getInt("semester"), rs.getInt("faculty_id")));
		return list;
	}

	public void setValues(int id, String subject, String time, String room, String filepath, int sem, int facultyId) {
		this.id = id;
		this.title = subject; 
		this.time = time;
		this.room = room;
		this.syllabusFile = filepath;
		this.sem = sem;
		this.facultyId = facultyId;
	}

	public void removeFromDatabase() throws Exception {
		String update_query = "delete from subject where id = "+id;
		Home_Controller.sqlConnect.update(update_query);
	}
}
