package model.faculty.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import controller.Home_Controller;
import model.SQLConnection;

public class Publication extends Entity {

	public static ArrayList<Publication> list = new ArrayList<>();
	public String title;
	public String filepath;
	public int id;
	public int facultyId;
	public int type;
	public String date_published;
	
	public String prompt;
	
	public Publication(int id, int type, String title, Date date_published, String filename, int facultyId){
		this.id = id;
		this.facultyId = facultyId;
		this.type = type;
		this.title = title;
		this.filepath = filename;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		this.date_published = formatter.format(date_published);
	}
	
	public boolean addOrUpdate(int choice){
		prompt = "";
		
		if(choice == 0){
			try{
				String insert_publication = "INSERT INTO `publication`(title, filename, type, faculty_id, datePublished) VALUES "
						+ "('" + SQLConnection.insertBackSlash(title) + "','" + SQLConnection.insertBackSlash(filepath) + "','" 
						+ type + "'," + facultyId + ",'"+date_published+"');\n";
				
				Home_Controller.sqlConnect.update(insert_publication);
				
			} catch(Exception e){
				e.printStackTrace();
				prompt = "There is an error adding a publication!";
				return false;
			}
			
			prompt = "Successfully added a new activity!";
			return true;
		}
		
		else if(choice == 1){
			String update_publication = "UPDATE publication SET title = '" + SQLConnection.insertBackSlash(title) + "', filename = '" + SQLConnection.insertBackSlash(filepath)
					+ "', type = " + type + ", datePublished = '"+date_published+"' WHERE id =" + id + ";";
			
			try{
				Home_Controller.sqlConnect.update(update_publication);
			} catch (Exception e){
				e.printStackTrace();
				prompt = "There was an error encountered while editing the publication!";
				return false;
			}
			prompt = "Successfully edited the publication!";
			return true;
		}
		return false;
	}
	
	public static ArrayList<Publication> getPublications(int facultyId) throws ClassNotFoundException, SQLException {
		String query = "select * from publication where faculty_id = "+facultyId+" order by datePublished desc";
		ResultSet rs = Home_Controller.sqlConnect.query(query);
		ArrayList<Publication> list = new ArrayList<>();
		while(rs.next())
			list.add(new Publication(rs.getInt("id"), rs.getInt("type"), rs.getString("title"), rs.getDate("datePublished"), rs.getString("filename"), rs.getInt("faculty_id")));
		return list;
	}

	public void setValues(int id, int facultyId, String filepath, String title, int type, Date date) {
		this.id = id;
		this.facultyId = facultyId;
		this.filepath = filepath;
		this.title = title;
		this.type = type;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		this.date_published = formatter.format(date);
	}

	public void removeFromDatabase() throws Exception {
		String update_query = "delete from publication where id = "+id;
		Home_Controller.sqlConnect.update(update_query);
	}
}
