package model.faculty.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import controller.Home_Controller;
import model.SQLConnection;

public class Activity extends Entity {

	public static ArrayList<Activity> list;
	public String title;
	public String venue;
	public int id;
	public int facultyId;
	public int type;
	public String date;
	
	public String prompt;
	
	public Activity(int id, int facultyId, String venue, String title, int type, Date date){
		this.id = id;
		this.facultyId = facultyId;
		this.type = type;
		this.title = title;
		this.venue = venue;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		this.date = formatter.format(date);
	}
	
	/**
	 * if choice is 1, then add otherwise update
	 * @param choice
	 * @param title
	 * @param venue
	 * @param type
	 * @param facultyId
	 * @param row
	 * @return
	 */
	public boolean addOrUpdate(int choice){
		prompt = "";
		
		if(choice == 0){
			try{
				String insert_activity = "INSERT INTO `activity`(title, venue, type, facultyID, date) VALUES "
						+ "('" + SQLConnection.insertBackSlash(title) + "','" + SQLConnection.insertBackSlash(venue) + "','" 
						+ type + "'," + facultyId + ", '"+date+"');\n";
				
				Home_Controller.sqlConnect.update(insert_activity);
				
			} catch (Exception e){
				e.printStackTrace();
				prompt = "There is an error adding an activity!";
				return false;
			}
			
			prompt = "Successfully added a new activity!";
			return true;
		}
		
		else if(choice == 1){
			
			String update_activity = "UPDATE activity SET title = '" + SQLConnection.insertBackSlash(title) + "', venue = '" + SQLConnection.insertBackSlash(venue) 
					+ "', type = " + type + ", date = '"+date+"' WHERE id =" + id + ";";
			System.out.println(update_activity);
			try{
				Home_Controller.sqlConnect.update(update_activity);
			} catch (Exception e){
				e.printStackTrace();
				prompt = "There was an error encountered while updating the activity!";
				return false;
			}
			prompt = "Successfully edited the activity!";
			return true;
		} 
		return false;
	}
	
	public static ArrayList<Activity> getActivities(int facultyId) throws ClassNotFoundException, SQLException {
		String query = "select * from activity where facultyID = "+facultyId+" order by date desc";
		ResultSet rs = Home_Controller.sqlConnect.query(query);
		ArrayList<Activity> list = new ArrayList<>();
		while(rs.next())
			list.add(new Activity(rs.getInt("id"), facultyId, rs.getString("venue"), rs.getString("title"), rs.getInt("type"), rs.getDate("date")));
		return list;
	}

	public void setValues(int id, int facultyId, String venue, String title, int type, Date date) {
		this.id = id;
		this.facultyId = facultyId;
		this.venue = venue;
		this.title = title;
		this.type = type;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		this.date = formatter.format(date);
	}

	public void removeFromDatabase() throws Exception {
		String update_query = "delete from activity where id = "+id;
		Home_Controller.sqlConnect.update(update_query);
	}
}
