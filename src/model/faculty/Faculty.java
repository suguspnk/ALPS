package model.faculty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import model.SQLConnection;
import controller.Home_Controller;

public class Faculty {
	
	public String fName,mName,lName,addr,bdate,conOne,conTwo,email,degree,position,status,path, user_id;
	public int fid,id,stat_int,age;
	
	public String prompt;
	
	public Faculty(){
		
	}
	
	/**
	 * Create new faculty and store information
	 * parameters: faculty information
	 * @param user_id 
	 * 
	 */
	public Faculty(int id,String fName,String mName,String lName,String addr,String bdate,int age, String conOne,String conTwo, String email, String degree,String position, String path, int status, String user_id){
		
		this.id = id;
		this.fName = fName;
		this.mName = mName;
		this.lName = lName;
		this.addr = addr;
		this.bdate = bdate;
		this.age = age;
		this.conOne = conOne;
		this.conTwo = conTwo;
		this.email = email;
		this.degree = degree;
		this.position = position;
		this.path = path;
		this.stat_int = status;
		this.user_id = user_id;
	}
	
	/**
	 * Query to add or edit faculty record
	 * parameters: ch(flag to indicate if add or edit),faculty information and row or index in facList
	 * 
	 */
	public boolean addOrUpdate(int ch,String fName,String mName,String lName,String addr,String bdate,String conOne,String conTwo, String email, String degree,String position, String status, String path, int row){
		
		prompt = "";
		//define
		this.fName = fName;
		this.mName = mName;
		this.lName = lName;
		this.addr = addr;
		this.bdate = bdate;
		this.conOne = conOne;
		this.conTwo = conTwo;
		this.email = email;
		this.degree = degree;
		this.position = position;
		this.status = status;
		this.path = path.replace('\\', '/');
		
		stat_int = (status.equals("Active")? 0: (status.equals("On Leave")? 1 : 2)); 
		
		//Add faculty
		if(ch == 1){
			fid = 0;
			try {
				
				//Insert new Faculty
				String insert_fac ="INSERT INTO `faculty`(first_name,middle_name,surname,address,birthdate,degree,position,status,photo,user_id) VALUES "
						+ "('"+SQLConnection.insertBackSlash(fName)+"','"+SQLConnection.insertBackSlash(mName)+"','"+ SQLConnection.insertBackSlash(lName)+"','"
						+SQLConnection.insertBackSlash(addr) +"', '"+SQLConnection.insertBackSlash(bdate) + "','" +SQLConnection.insertBackSlash(degree) + "','" + 
						SQLConnection.insertBackSlash(position)+ "'," + stat_int + ",'" + this.path + "',"+ Integer.parseInt(Home_Controller.user.getId()) +");\n";
				//Get faculty ID
				String get_fid = "SELECT ID from faculty WHERE first_name = '" + SQLConnection.insertBackSlash(fName) +"' AND surname = '" + 
						SQLConnection.insertBackSlash(lName)+ "' AND middle_name='" + SQLConnection.insertBackSlash(mName) +"';\n";
								
				Home_Controller.sqlConnect.update(insert_fac);
				ResultSet rs = Home_Controller.sqlConnect.query(get_fid);
				if(rs.next())
					fid = rs.getInt("ID");
				
				//Insert contact_number 1 and 2
				String insert_con1 = "INSERT INTO `contact_number`(number,faculty_id) VALUES('" + SQLConnection.insertBackSlash(conOne) + "'," + fid +");\n" ;
				String insert_con2 = conTwo.isEmpty()? "" : "INSERT INTO `contact_number`(number,faculty_id) VALUES('" + SQLConnection.insertBackSlash(conTwo) + "'," + fid + ");\n" ;
				
				//Insert email
				String insert_email = "INSERT INTO `email`(email,faculty_id) VALUES"
						+ "('" + SQLConnection.insertBackSlash(email) + "', " + fid + ");" ;
				
				//execute batch sana, error when executing update with multiple insert
				Home_Controller.sqlConnect.update(insert_con1);
				if(!conTwo.isEmpty())
					Home_Controller.sqlConnect.update(insert_con2);
				Home_Controller.sqlConnect.update(insert_email);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				prompt = "Oops. There is an error adding a new faculty.";
				return false;
			}
			
			prompt = "Successfully added new faculty!";
			return true;
		}
		
		//Edit faculty
		else if(ch == 2){
			
			fid = Faculty_List.getInstance().facList.get(row).id;
			
			String update_fac = "UPDATE faculty SET first_name = '" + SQLConnection.insertBackSlash(fName) + "', middle_name = '" + SQLConnection.insertBackSlash(mName) +"', surname = '" 
			+ SQLConnection.insertBackSlash(lName) + "', address = '" +  SQLConnection.insertBackSlash(addr) + "', birthdate = '" + SQLConnection.insertBackSlash(bdate) +"', degree = '" 
			+ SQLConnection.insertBackSlash(degree) +"', position = '" +  SQLConnection.insertBackSlash(position) + (!this.path.isEmpty()? "', photo = '" + this.path : "")+ "', status =" + stat_int + " WHERE id =" + fid + ";" ;
			
			String update_conOne = "UPDATE contact_number SET number = '" + SQLConnection.insertBackSlash(conOne) + "' WHERE faculty_id = " + fid + " AND number = '" + Faculty_List.getInstance().facList.get(row).conOne +"' ;";
			String update_conTwo = "";
			
			if(!Faculty_List.getInstance().facList.get(row).conTwo.isEmpty() && !conTwo.isEmpty())
				update_conTwo = "UPDATE contact_number SET number = '" + SQLConnection.insertBackSlash(conTwo) + "' WHERE faculty_id = " + fid + " AND number = '" + Faculty_List.getInstance().facList.get(row).conTwo +"' ;";
			else if(Faculty_List.getInstance().facList.get(row).conTwo.isEmpty() && !conTwo.isEmpty())
				update_conTwo =  "INSERT INTO `contact_number`(number,faculty_id) VALUES('" + SQLConnection.insertBackSlash(conTwo) + "'," + fid + ");\n";
			else if(!Faculty_List.getInstance().facList.get(row).conTwo.isEmpty() && conTwo.isEmpty()){
				update_conTwo = "DELETE FROM contact_number WHERE faculty_id =" + fid + " AND number = '" + Faculty_List.getInstance().facList.get(row).conTwo + "';";
			}
			
			String update_email = "UPDATE email SET email = '" + SQLConnection.insertBackSlash(email) + "' WHERE faculty_id = " + fid + ";";
			
			try {
				Home_Controller.sqlConnect.update(update_fac);
				Home_Controller.sqlConnect.update(update_conOne);
				if(!update_conTwo.equals(""))
					Home_Controller.sqlConnect.update(update_conTwo);
				Home_Controller.sqlConnect.update(update_email);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				prompt = "Oops. There was an error encountered while editing the faculty profile";
				return false;
			}
			
			prompt = "Successfully edited faculty profile!";
			return true;
		}
		
		return false;
		
	}
	
	/**
	 * Queary to deactivate or delete faculty record
	 * @param id
	 * @return
	 */
	public boolean deactivate(int id){
		
		try {
			Home_Controller.sqlConnect.update("DELETE from faculty WHERE id = " + id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			prompt = "Oops. There was an error encountered while deleting the faculty's records";
			return false;
		}
		
		prompt = "Successfully deleted faculty profile!";
		return true;
	}
	
	/**
	 * Query to check if faculty exist in db
	 * @param fName
	 * @param mName
	 * @param lName
	 * @return
	 */
	public boolean checkIF_FacultyExist(String fName,String mName, String lName){
		
		String check = "SELECT Count(*) as count from faculty WHERE first_name = '" + SQLConnection.insertBackSlash(fName) +"' AND surname = '" + 
				SQLConnection.insertBackSlash(lName)+ "' AND middle_name='" + SQLConnection.insertBackSlash(mName) +"';\n";
		
		ResultSet rs;
		try {
			rs = Home_Controller.sqlConnect.query(check);
			if(rs.next()){
				if(rs.getInt("count")!=0)
					return true;
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean checkIF_EmailExists(String email, int id){
		
		String check = "SELECT id from email WHERE email = '" + SQLConnection.insertBackSlash(email) + "'";
		
		if(id !=-1)
			check = check + " AND faculty_id != " + id + ";";
		
		ResultSet rs;
		try {
			rs = Home_Controller.sqlConnect.query(check);
			if(rs.next())
				return true;
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static Vector<String> getFaculties() throws ClassNotFoundException, SQLException {
		ResultSet rs = Home_Controller.sqlConnect.query("select surname, first_name from faculty"); 
		Vector<String> vect = new Vector<String>();
		while(rs.next())
			vect.addElement(rs.getString("surname")+", "+rs.getString("first_name"));
		return vect;
	}
	

}