package model.faculty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.SQLConnection;
import controller.Home_Controller;

public class Faculty_List {
	
	private static Faculty_List facultyList;
	
	public ArrayList<Faculty> facList = new ArrayList<Faculty>();
	public String searchOpt="", key="";
	public int page = 0;
	
	public Faculty_List(){
		
	}
	
	/**
	 * Get all faculty records and store it to facList
	 */
	public void update_FacultyList(){
		
		if(page*20<0)
			return;
		
		if(!facList.isEmpty())
			facList.clear();
		
		String query = "SELECT faculty.first_name,faculty.middle_name,faculty.surname, faculty.address,faculty.birthdate,faculty.degree,faculty.position,faculty.status,TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) AS age, faculty.user_id,"
				+ "faculty.photo, faculty.id, email.email FROM faculty,email WHERE faculty.id = email.faculty_id ORDER BY surname,first_name,middle_name LIMIT " + page*20 + ", 20;" ;
		
		ResultSet rs;
		try {
			rs = Home_Controller.sqlConnect.query(query);
			
			while(rs.next()){
				
				int id = rs.getInt("faculty.id");
				String fName = rs.getString("faculty.first_Name");
				String mName = rs.getString("faculty.middle_Name");
				String lName = rs.getString("faculty.surname");
				String addr = rs.getString("faculty.address");
				String bdate = rs.getDate("faculty.birthdate").toString();
				int age = rs.getInt("age");
				String email = rs.getString("email.email");
				String degree = rs.getString("faculty.degree");
				String position = rs.getString("faculty.position");
				String path = rs.getString("faculty.photo");
				String user_id = ""+rs.getInt("faculty.user_id");
				int status = rs.getInt("faculty.status");
				
				query = "SELECT number FROM contact_number WHERE faculty_id =" + rs.getInt("faculty.id");
				ResultSet nums = Home_Controller.sqlConnect.queryTwo(query);
				
				String numbers[] = {"",""};
				
				int i = 0;
				while(nums.next()){
					numbers[i] = nums.getString("number");
					i++;
				}
				
				String conOne = numbers[0], conTwo = numbers.length>1? numbers[1] : "";
				
				facList.add(new Faculty(id,fName, mName, lName, addr, bdate, age, conOne, conTwo, email, degree, position, path, status, user_id));
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	/***
	 * Do search query depending on search option and update facList
	 */
	public void update_searchList(){
		
		if(page*20<0)
			return;
		
		if(!facList.isEmpty())
			facList.clear();
		
		String search_query = "";
		

		if(searchOpt.equals("number")){
			
			search_query = "SELECT number,faculty_id from contact_number WHERE number =" +  SQLConnection.insertBackSlash(key);
			
			ResultSet nums;
			int fid = 0;
			try {
				nums = Home_Controller.sqlConnect.query(search_query);
				while(nums.next()){
					fid = nums.getInt("faculty_id");
				}
				
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			
			search_query = "SELECT faculty.first_name,faculty.middle_name,faculty.surname, faculty.address,faculty.birthdate,faculty.degree,faculty.position,faculty.status,TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) AS age, faculty.photo,faculty.id,faculty.user_id,"
					+ "email.email FROM faculty,email WHERE faculty.id = email.faculty_id AND faculty.id = " + fid + " ORDER BY surname,first_name,middle_name LIMIT " + page*20 + ", 20;";
		}
		
		else
			search_query = "SELECT faculty.first_name,faculty.middle_name,faculty.surname, faculty.address,faculty.birthdate,faculty.degree,faculty.position,faculty.status,TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) AS age, faculty.photo,faculty.id, faculty.user_id,"
				+ "email.email FROM faculty,email WHERE faculty.id = email.faculty_id AND " + searchOpt + " LIKE '%" + SQLConnection.insertBackSlash(key) + "%' ORDER BY surname,first_name,middle_name LIMIT " + page*20 + ", 20;";
		
		ResultSet rs;
		try {
			rs = Home_Controller.sqlConnect.query(search_query);
			
			while(rs.next()){
				
				int id = rs.getInt("faculty.id");
				String fName = rs.getString("faculty.first_Name");
				String mName = rs.getString("faculty.middle_Name");
				String lName = rs.getString("faculty.surname");
				String addr = rs.getString("faculty.address");
				String bdate = rs.getDate("faculty.birthdate").toString();
				int age = rs.getInt("age");
				String email = rs.getString("email.email");
				String degree = rs.getString("faculty.degree");
				String position = rs.getString("faculty.position");
				String path = rs.getString("faculty.photo");
				String user_id = ""+rs.getInt("faculty.user_id");
				int status = rs.getInt("faculty.status");
				
				search_query = "SELECT number FROM contact_number WHERE faculty_id =" + id;
				ResultSet nums = Home_Controller.sqlConnect.queryTwo(search_query);
				
				String numbers[] = {"",""};
				
				int i = 0;
				while(nums.next()){
					numbers[i] = nums.getString("number");
					i++;
				}
				
				String conOne = numbers[0], conTwo = numbers.length>1? numbers[1] : "";
				
				facList.add(new Faculty(id,fName, mName, lName, addr, bdate, age, conOne, conTwo, email, degree, position, path, status, user_id));
				
			}
				
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	public Faculty getRecord(int row){
		
		return facList.get(row);
	}
	
	public int get_NumPages(String query){
		
		int pages = 0;
		try {
			ResultSet rs = Home_Controller.sqlConnect.query(query);
			while(rs.next()){
				pages = rs.getInt("count");
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pages/20 + (pages%20==0? 0 : 1);
	}
	
	public static Faculty_List getInstance(){
		
		if(facultyList == null)
			facultyList = new Faculty_List();
		return facultyList;
	}
}
