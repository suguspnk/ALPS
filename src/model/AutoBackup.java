package model;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import controller.Home_Controller;

public class AutoBackup implements Runnable{
	
	public AutoBackup(){
		Thread thread = new Thread(this);
		thread.start();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		String backupFolderPath = javax.swing.filechooser.FileSystemView.getFileSystemView().getDefaultDirectory().getAbsolutePath()+"/eFRMS/backup";
		try {
			createBackupFolder(backupFolderPath);
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(null, "An error occured when creating the backup folder.", "Error", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		}
		DateFormat dateFormatter = new SimpleDateFormat("MMM dd, yyyy HH-mm");
		Date date = new Date();
		String currentDateTime = dateFormatter.format(date);
		int hour = date.getHours();
		int min = date.getMinutes();
		int sec = date.getSeconds();
		long sleeptime = calculateSleepTime(hour, min, sec);
		if(sleeptime == -1)
			return;
		try {
			Thread.sleep(sleeptime);
			currentDateTime = dateFormatter.format(new Date());
			Home_Controller.createBackup(backupFolderPath+"/"+currentDateTime+"_backup.sql");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void createBackupFolder(String backupFolderPath) throws IOException {
		File file = new File(backupFolderPath);
		if(!file.exists() || !file.isDirectory())
			file.mkdirs();
	}

	private long calculateSleepTime(int hour, int min, int sec) {
		if(hour >= 18)
			return -1;
		if(hour == 12 || hour == 18)
			return 0;
		int hourmins = 0;
		if(hour < 12)
			hourmins = (11-hour)*60;
		else
			hourmins = (17-hour)*60;
		hourmins += 59-min;
		long sleepSecs = hourmins*60+(60-sec);
		return sleepSecs*1000;
	}
}