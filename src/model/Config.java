package model;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;

public class Config {
	
	// database config
	public static String database = "alps_db";
	public static String hostname = "localhost";
	public static String user = "root";
	public static String port = "3306";
	public static String password;
	
	// user config
	public static String personnelName = "Ate Lainee";
	public static String personnelDescription = "DNSM Personnel";
	
	// application config
	public static String title = "A.L.P.S";
	public static String titleMeaning = "Ate Lainee's Personal System";
	
	public static int additionalFrameSize = 0;
	public static String operating_system = "";
	
	public static void initConfig() throws InvalidFileFormatException, IOException{
		File file = new File("config.ini");
		if(!file.exists())
			createConfig(file);
		Wini ini = new Wini(file);
		readDatabaseConfig(ini);
		readUserConfig(ini);
		readApplicationConfig(ini);
	}

	public static void detectAdditionFrameSize() {
		String OS = System.getProperty("os.name").toLowerCase();
		System.out.println(OS);
		if (isWindows(OS)) {
			System.out.println("This is Windows");
			operating_system = "windows";
		} else if (isMac(OS) || isUnix(OS) || isSolaris(OS)) {
			System.out.println("This is Mac | Unix | Solaris");
			operating_system = "linux";
		} else {
			System.out.println("Your OS is not support!!");
			operating_system = "others";
		}
	}
 
	public static boolean isWindows(String OS) {
		return (OS.indexOf("win") >= 0);
	}
 
	public static boolean isMac(String OS) {
		return (OS.indexOf("mac") >= 0);
	}
 
	public static boolean isUnix(String OS) {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
	}
 
	public static boolean isSolaris(String OS) {
		return (OS.indexOf("sunos") >= 0);
	}

	private static void readDatabaseConfig(Wini ini) {
		database = ini.get("database", "name");
		hostname = ini.get("database", "hostname");
		user = ini.get("database", "user");
		port = ini.get("database", "port");
	}

	private static void readUserConfig(Wini ini) {
		personnelName = ini.get("user", "name");
		personnelDescription = ini.get("user", "description");
	}

	private static void readApplicationConfig(Wini ini) {
		title = ini.get("application", "title");
		titleMeaning = ini.get("application", "meaning");
	}

	private static void createConfig(File file) throws IOException {
		file.createNewFile();
		PrintWriter writer = new PrintWriter(file);
		writer.print("[database]\n"
				+ "name = alps_db\n"
				+ "hostname = localhost\n"
				+ "user = root\n"
				+ "port = 3306\n\n"
				+ "[user]\n"
				+ "name = Ate Lainee\n"
				+ "description = DNSM Personnel\n\n"
				+ "[application]\n"
				+ "title = A.L.P.S\n"
				+ "meaning = Ate Lainee's Personal System");
		writer.close();
	}
}
