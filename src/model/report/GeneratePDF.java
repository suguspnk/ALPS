package model.report;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.SQLConnection;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class GeneratePDF {

	public void createPdf(String filename, Object[] data)
	        throws IOException, DocumentException {
			FileOutputStream fileStream = new FileOutputStream(filename);
	    	// step 1
	        Document document = new Document();
	        // step 2
	        PdfWriter.getInstance(document, fileStream);
	        // step 3
	        document.open();
	        // step 4
	        PdfPTable table = createTable1(data);
	        document.add(table);
	        // step 5
	        document.close();
	        fileStream.close();
	    }
	    
	    /**
	     * Creates a table; widths are set with setWidths().
	     * @return a PdfPTable
	     * @throws DocumentException
	     */
	    public PdfPTable createTable1(Object[] data) throws DocumentException {
	        PdfPTable table = new PdfPTable(4);
	        table.setWidthPercentage(288 / 5.23f);
	        table.setWidths(new int[]{2, 2, 2, 2});
	        PdfPCell cell;
	        cell = new PdfPCell(new Phrase(""+data[1]));
	        cell.setColspan(4);
	        table.addCell(cell);
	        table.addCell("Venue");
	        table.addCell("Title");
	        table.addCell("Type");
	        String[][] entry = getActivity(data);
	        
	        table.addCell("Date");
	        for(int j = 0 ; j < entry.length ; j++){
	        	for( int k = 0 ; k < 4 ; k++){
		       		table.addCell("" + entry[j][k]);
	        	}
	       	}
	        return table;
	    }
	    
	    private String[][] getActivity(Object[] data){
	    	String[][] result = new String[1][4];
	    	
	    	SimpleDateFormat old_format = new SimpleDateFormat("MMM-dd-yyyy");
	    	SimpleDateFormat new_format = new SimpleDateFormat("MM-dd-yyyy");
	    	java.sql.Date sqlfromDate = new java.sql.Date(01-01-2001);
	    	java.sql.Date sqltoDate = new java.sql.Date(01-01-2002); 
	    	
	    	String from, to;
			try {
				System.out.println(data[2] + " : " + data[3]);
				Date fromD = old_format.parse((String) data[2]);
				old_format.applyPattern("MM-dd-yyyy");
				from = old_format.format(fromD);
				//System.out.println(from);
				Date fromdate = new_format.parse(from);
				//Date fromdate = format.parse((String) data[2]);
				
				old_format = new SimpleDateFormat("MMM-dd-yyyy");
				Date toD = old_format.parse((String) data[3]);
				old_format.applyPattern("MM-dd-yyyy");
				to = old_format.format(toD);
				Date todate = new_format.parse(to);
				//System.out.println(""+todate);
				//System.out.println(""+fromdate);
				
				sqlfromDate = new java.sql.Date(fromdate.getTime());
				sqltoDate = new java.sql.Date(todate.getTime());
		
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			System.out.println(sqlfromDate);
			
	    	String query = "SELECT * from activity WHERE (date BETWEEN '"+sqlfromDate+"' AND '"+sqltoDate+"')";
	    	try {
				int i  = 0 ;
				ResultSet res = SQLConnection.getInstance().query(query);
				while(res.next()){
					i++;
				}
				res.beforeFirst();
				result = new String[i][4];
				
				i = 0;
				while(res.next()){
	        		result[i][0] = res.getString("venue");
	        		result[i][1] = res.getString("title");
	        		result[i][2] = ""+ res.getInt("type");
	        		Date d = new Date(res.getTimestamp("date").getTime());
	        		result[i][3] = new SimpleDateFormat("MMM-dd-yyyy").format(d);
					
	        		i++;	
	        		//System.out.println(res.getTimestamp("date") + " : sookah : " + res.getInt("type"));
	        	}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	    	return result;
	    }
	    
	    
	
}
