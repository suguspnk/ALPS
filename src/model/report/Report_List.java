package model.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.SQLConnection;

public class Report_List {
	
	//ArrayList<Report> list = new ArrayList<Report>();
	public int[] id;
	
	public Object[][] get(int type, String str, int page){
		Object[][] data = new Object[1][1];
		str = str.trim();
		String query = "SELECT * FROM report";
		if(str.isEmpty()){
			
		}
		else if(type == 1 && isNumeric(str)){
			query = query + " WHERE type LIKE "+str+"";
			System.out.println("Type");
		}
		else if(type == 2){
			query = query + " WHERE lower_range LIKE '%"+str+"%'";
			System.out.println("lower_range");
		}
		else if(type == 3){
			query = query + " WHERE upper_range LIKE '%"+str+"%'";
			System.out.println("upper_range");
		}
		
		
		try {
			int i  = 0 ;
			ResultSet res = SQLConnection.getInstance().query(query);
			while(res.next()){
				i++;
			}
			res.beforeFirst();
			data = new Object[i][5];
			if(i>20)
				data = new Object[20][5];
			id = new int[i];
			
			if(page > 0 ){
				query = query + " LIMIT 20 OFFSET "+ page*20;
				res = SQLConnection.getInstance().query(query);
			}
			
			i = 0;
			while(res.next()){
				id[i+(page*20)] = res.getInt("id");
				if(i<20){
	        		data[i][0] = i+1+(page*20);
	        		
	        		int resType = res.getInt("type");
	        		
	        		if(resType == 0)	
	        			data[i][1] = "General Report";
	        		else
	        			data[i][1] = "Specific Report";
	        		
	        		Date d = new Date(res.getTimestamp("lower_range").getTime());
	        		data[i][2] = new SimpleDateFormat("MMM-dd-yyyy").format(d);
	        		d = new Date(res.getTimestamp("upper_range").getTime());
	        		data[i][3] = new SimpleDateFormat("MMM-dd-yyyy").format(d);
	        		data[i][4] = "View";
				}
        		i++;	
        		//System.out.println(res.getTimestamp("date") + " : sookah : " + res.getInt("type"));
        	}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return data;
	}
	
	private boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
	
}
