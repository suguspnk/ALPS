package model.report;

import java.util.Date;

import model.SQLConnection;

public class Report {
	private int type;
	private Date date;
	private String requestor;
	private Date lower_range;
	private Date upper_range;
	
	
	public Report(){
		
	}
	
	public Report( int type, Date date, String requestor, Date lower_range, Date upper_range){
		this.type = type;
		this.date = date;
		this.requestor = requestor;
		this.lower_range = lower_range;
		this.upper_range = upper_range;
		System.out.println(type + " " + date + " " + requestor + " " + lower_range + " " + upper_range);
	}
	
	public void delete(int id){
		String query = "DELETE FROM report WHERE id="+id ;
		try {
			SQLConnection.getInstance().update(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void save(){
		
		//String query = "INSERT INTO report ( `id`, `type`, `date`, `requestor`, `lower_range`, `upper_range`, `user_id`) VALUES ( 1, '"+type+"', '"+date+"', '"+ requestor +"', '"+ lower_range +"', '"+upper_range+"' , 2);";
		String query = "INSERT INTO report ( `type`, `date`, `requestor`, `lower_range`, `upper_range`, `user_id`) VALUES ( '"+type+"', '"+date+"', '"+ requestor +"', '"+ lower_range +"', '"+upper_range+"' , 1);";
			try {
				SQLConnection.getInstance().update(query);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
	}

	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public Date getLower_range() {
		return lower_range;
	}

	public void setLower_range(Date lower_range) {
		this.lower_range = lower_range;
	}

	public Date getUpper_range() {
		return upper_range;
	}

	public void setUpper_range(Date upper_range) {
		this.upper_range = upper_range;
	}
	 
}
