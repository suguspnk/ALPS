package view;

import javax.swing.JButton;
import javax.swing.JDialog;

import controller.Home_Controller;

@SuppressWarnings("serial")
public abstract class Form extends JDialog{
	
	public JButton btnAdd;
	public JButton btnCancel;
	
	public Form() {
		super(Home_Controller.homeFrame, true);
		
		btnAdd = new JButton("Add");
		btnCancel = new JButton("Cancel");
		
		setLayout(null);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	}
	
	/**
	 * verify the inputs in the form if the inputs are correct. if the inputs are correct, the method will return true otherwise false
	 * @return
	 */
	public abstract boolean verifyInput();
}