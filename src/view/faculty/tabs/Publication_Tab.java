package view.faculty.tabs;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import view.faculty.forms.Publication_Form;
import model.faculty.entities.Publication;

@SuppressWarnings("serial")
public class Publication_Tab extends JPanel{
	
	private static Publication_Tab publicationTab;
	
	public JTable publicationTable;
	public JButton addButton;
	public JButton editButton;
	public JButton deleteButton;
	public JScrollPane scrollPane;
	public DefaultTableModel tableModel = new DefaultTableModel(0,0);
	public TableColumnModel columnModel;
	public JCheckBox checkAll;
	public static final Object columnNames[] = {" ", "Title", "Date", "Type", "File"};
	
	public Publication_Tab(){
		
		super();
		initComponents();
		addComponents();
		setPanelLayout();
	}
	
	private void addComponents(){
		add(addButton);
		add(editButton);
		add(deleteButton);
	}
	
	public void setPanelLayout(){
		setLayout(null);
		setSize(379, 414);
		setVisible(false);
	}
	
	public static Publication_Tab getInstance(){
		if(publicationTab == null)
			publicationTab = new Publication_Tab();
		return publicationTab;
	}
	
	public void initComponents(){
		
		addButton = new JButton("Add");
		addButton.setBounds(195, 5, 50, 30);
		
		editButton = new JButton("Edit");
		editButton.setBounds(245, 5, 50, 30);
		
		deleteButton = new JButton("Delete");
		deleteButton.setBounds(295, 5, 65, 30);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 40, 350 , 360);
		
		checkAll = new JCheckBox("Check All");
		checkAll.setBounds(15, 18, 79,17);
		
		add(checkAll);
		
		add(scrollPane);
		
		publicationTable = new JTable(){
			
			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return Boolean.class;
                    case 1:
                        return String.class;
                    case 2:
                        return String.class;
                    case 3:
                        return String.class;
                    default:
                        return String.class;
                }
            }

			
			public boolean isCellEditable(int row, int column){
				return false;
			};
			
			public String getToolTipText(MouseEvent e) {
                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);

                try {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } catch (RuntimeException e1) {
                    //catch null pointer exception if mouse is over an empty line
                }
                return tip;
            }
			
			DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

		    { // initializer block
		        renderRight.setHorizontalAlignment(SwingConstants.CENTER);
		    }
		};
		
		tableModel.setColumnIdentifiers(columnNames);
		publicationTable.setModel(tableModel);
		publicationTable.setFillsViewportHeight(true);
		publicationTable.getTableHeader().setReorderingAllowed(false);
		publicationTable.setRowHeight(25);
		publicationTable.setSelectionBackground(Color.DARK_GRAY);
		publicationTable.setSelectionForeground(Color.WHITE);
		publicationTable.setShowGrid(true);
		publicationTable.setAutoCreateRowSorter(true);
		publicationTable.getTableHeader().setResizingAllowed(false);
		publicationTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		publicationTable.getColumnModel().getColumn(0).setPreferredWidth(20);
		columnModel = publicationTable.getColumnModel();
		scrollPane.setViewportView(publicationTable);
	}
	
	public void showPanel(){
		setVisible(true);
	}
	
	public void updateTable(ArrayList<Publication> publications) {
		Publication.list = publications;
		Object data[][] = new Object[publications.size()][columnNames.length];
		for(int i = 0; i < publications.size(); i++){
			Publication publication = publications.get(i);
			data[i] = new Object[]{new Boolean(false), publication.title, publication.date_published, Publication_Form.publication_types[publication.type], publication.filepath}; 
		}
		DefaultTableModel model = new DefaultTableModel(data, columnNames);
		publicationTable.setModel(model);
		publicationTable.getColumnModel().getColumn(0).setPreferredWidth(20);
		checkAll.setEnabled(!publications.isEmpty());
		checkAll.setSelected(false);
	}


}