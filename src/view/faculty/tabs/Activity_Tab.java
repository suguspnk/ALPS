package view.faculty.tabs;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import model.faculty.entities.Activity;

@SuppressWarnings("serial")
public class Activity_Tab extends JPanel{
	
	private static Activity_Tab activityTab;
	
	public JTable activityTable;
	public JButton addButton;
	public JButton editButton;
	public JButton deleteButton;
	public JScrollPane scrollPane;
	public DefaultTableModel tableModel = new DefaultTableModel(0,0);
	public TableColumnModel columnModel;
	public JCheckBox checkAll;
	public final Object columnNames [] = {" ", "Title", "Date", "Venue", "Type"};
	
	public Activity_Tab(){
		
		super();
		initComponents();
		addComponents();
		setPanelLayout();
	}
	
	private void addComponents(){
		add(addButton);
		add(editButton);
		add(deleteButton);
	}
	
	public void setPanelLayout(){
		setLayout(null);
		setSize(378, 415);
		setVisible(false);
	}
	
	public static Activity_Tab getInstance(){
		if(activityTab == null)
			activityTab = new Activity_Tab();
		return activityTab;
	}
	
	public void initComponents(){
		
		addButton = new JButton("Add");
		addButton.setBounds(195, 5, 50, 30);
		
		editButton = new JButton("Edit");
		editButton.setBounds(245, 5, 50, 30);
		
		deleteButton = new JButton("Delete");
		deleteButton.setBounds(295, 5, 65, 30);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 40, 350 , 360);
		
		checkAll = new JCheckBox("Check All");
		checkAll.setBounds(15, 18, 79,17);
		
		add(checkAll);
		add(scrollPane);
		
		activityTable = new JTable(){
			
			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return Boolean.class;
                    case 1:
                        return String.class;
                    case 2:
                        return String.class;
                    case 3:
                        return String.class;
                    default:
                        return String.class;
                }
            }

			
			public boolean isCellEditable(int row, int column){
				return false;
			};
			
			public String getToolTipText(MouseEvent e) {
                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);

                try {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } catch (RuntimeException e1) {
                    //catch null pointer exception if mouse is over an empty line
                }
                return tip;
            }
			
			DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

		    { // initializer block
		        renderRight.setHorizontalAlignment(SwingConstants.CENTER);
		    }
		};
		
		tableModel.setColumnIdentifiers(columnNames);
		activityTable.setModel(tableModel);
		activityTable.setFillsViewportHeight(true);
		activityTable.getTableHeader().setReorderingAllowed(false);
		activityTable.setRowHeight(25);
		activityTable.setSelectionBackground(Color.DARK_GRAY);
		activityTable.setSelectionForeground(Color.WHITE);
		activityTable.setShowGrid(true);
		activityTable.setAutoCreateRowSorter(true);
		activityTable.getTableHeader().setResizingAllowed(false);
		activityTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		activityTable.getColumnModel().getColumn(0).setPreferredWidth(20);
		columnModel = activityTable.getColumnModel();
		scrollPane.setViewportView(activityTable);
		
	}
	
	public void showPanel(){
		setVisible(true);
	}
	
	/*
	 * public void showProfile(Faculty faculty){
		
		this.faculty = faculty;
		initFields();
		showFields(false);
		updateTabs(faculty.id);
		setVisible(true);
	}
	
	private void updateTabs(int faculty_id) {
		try {
			subjectTab.updateTable(Subject.getSubjects(faculty_id));
		} catch (ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(this, "An error occurred while getting the subjects of the faculty. The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		try {
			publicationTab.updateTable(Publication.getPublications(faculty_id));
		} catch (ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(this, "An error occurred while getting the publications of the faculty. The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		try {
			activityTab.updateTable(Activity.getActivities(faculty_id));
		} catch (ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(this, "An error occurred while getting the activities of the faculty. The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
*/
	public void updateTable(ArrayList<Activity> activities) {
		Activity.list = activities;
		Object data[][] = new Object[activities.size()][columnNames.length];
		for(int i = 0; i < activities.size(); i++){
			Activity activity = activities.get(i);
			data[i] = new Object[]{new Boolean(false), activity.title, activity.date, activity.venue, activity.type == 0 ? "University Service" : "Community Service"}; 
		}
		DefaultTableModel model = new DefaultTableModel(data, columnNames);
		activityTable.setModel(model);
		activityTable.getColumnModel().getColumn(0).setPreferredWidth(20);
		checkAll.setEnabled(!activities.isEmpty());
		checkAll.setSelected(false);
	}

}
