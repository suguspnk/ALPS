package view.faculty.tabs;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import model.faculty.entities.Subject;

@SuppressWarnings("serial")
public class Subject_Tab extends JPanel{

	private static Subject_Tab subjectTab;
	
	public JTable subjectTable;
	public JButton addButton;
	public JButton editButton;
	public JButton deleteButton;
	public JScrollPane scrollPane;
	public DefaultTableModel tableModel = new DefaultTableModel(0,0);
	public TableColumnModel columnModel;
	public JCheckBox checkAll;
	public static final String columnNames[] = {" ", "Title", "Time", "Room", "Sem", "Syllabus"};
	
	public Subject_Tab(){
		
		super();
		initComponents();
		addComponents();
		setPanelLayout();
	}
	
	private void addComponents(){
		add(addButton);
		add(editButton);
		add(deleteButton);
	}
	
	public void setPanelLayout(){
		setLayout(null);
		setSize(375,441);
		setVisible(true);
	}
	
	public static Subject_Tab getInstance(){
		if(subjectTab == null)
			subjectTab = new Subject_Tab();
		return subjectTab;
	}	
	public void initComponents(){
		
		addButton = new JButton("Add");
		addButton.setBounds(195, 5, 50, 30);
		
		editButton = new JButton("Edit");
		editButton.setBounds(245, 5, 50, 30);
		
		deleteButton = new JButton("Delete");
		deleteButton.setBounds(295, 5, 65, 30);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 40, 350 , 360);
		
		checkAll = new JCheckBox("Check All");
		checkAll.setBounds(15, 18, 79,17);
		
		add(checkAll);
		add(scrollPane);
		
		subjectTable = new JTable(){
			
			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return Boolean.class;
                    default:
                        return String.class;
                }
            }

			
			public boolean isCellEditable(int row, int column){
				return false;
			};
			
			public String getToolTipText(MouseEvent e) {
                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);

                try {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } catch (RuntimeException e1) {
                    //catch null pointer exception if mouse is over an empty line
                }
                return tip;
            }
			
			DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

		    { // initializer block
		        renderRight.setHorizontalAlignment(SwingConstants.CENTER);
		    }
		};
		
		tableModel.setColumnIdentifiers(columnNames);
		subjectTable.setModel(tableModel);
		subjectTable.setFillsViewportHeight(true);
		subjectTable.getTableHeader().setReorderingAllowed(false);
		subjectTable.setRowHeight(25);
		subjectTable.setSelectionBackground(Color.DARK_GRAY);
		subjectTable.setSelectionForeground(Color.WHITE);
		subjectTable.setShowGrid(true);
		subjectTable.setAutoCreateRowSorter(true);
		subjectTable.getTableHeader().setResizingAllowed(false);
		subjectTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		subjectTable.getColumnModel().getColumn(0).setPreferredWidth(20);
		columnModel = subjectTable.getColumnModel();
		scrollPane.setViewportView(subjectTable);

	}

	public void updateTable(ArrayList<Subject> subjects) {
		Subject.list = subjects;
		Object data[][] = new Object[subjects.size()][columnNames.length];
//		"Title", "Time", "Room", "Sem", "Syllabus"
		for(int i = 0; i < subjects.size(); i++){
			Subject subject = subjects.get(i);
			data[i] = new Object[]{new Boolean(false), subject.title, subject.time, subject.room, subject.sem+1, subject.syllabusFile}; 
		}
		DefaultTableModel model = new DefaultTableModel(data, columnNames);
		subjectTable.setModel(model);
		subjectTable.getColumnModel().getColumn(0).setPreferredWidth(20);
		checkAll.setEnabled(!subjects.isEmpty());
		checkAll.setSelected(false);
	}

}
