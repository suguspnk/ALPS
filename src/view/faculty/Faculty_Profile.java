package view.faculty;

import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

import model.faculty.Faculty;
import model.faculty.Faculty_List;
import model.faculty.entities.Activity;
import model.faculty.entities.Publication;
import model.faculty.entities.Subject;

import org.apache.commons.validator.routines.EmailValidator;

import com.toedter.calendar.JDateChooser;

import controller.Home_Controller;
import others.Util;
import view.faculty.tabs.Activity_Tab;
import view.faculty.tabs.Publication_Tab;
import view.faculty.tabs.Subject_Tab;

public class Faculty_Profile extends JDialog{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5969879804081572468L;

	private static Faculty_Profile instance;
	
	public JTextField fName,mName,lName,Address,age,contactOne,contactTwo,email,
	degree,position;
	public JComboBox<String> status;
	public JDateChooser bdate;
	private JLabel facImage,fNLbl,mNLbl,lNLbl,adLbl,bdLbl,ageLbl,conLbl,eLbl,degLbl,posLbl,statLbl;
	public JButton Edit,Delete,Save,Cancel,ch_img;
	private ImageIcon img = new ImageIcon("img/blankImg.png");
	
	public File file;
	public boolean hasChosen = false;
	
	public JTabbedPane tPane;
	
	public int current_row;
	private Faculty faculty;

	private Subject_Tab subjectTab;

	private Publication_Tab publicationTab;

	private Activity_Tab activityTab;
	
	public Faculty_Profile(){
		
		super(Home_Controller.homeFrame, true);
		
		initComponents();
		addComponents();
		
		setPanelLayout();
	}
	
	private void setPanelLayout(){
		
		setTitle("Faculty Profile");
		getContentPane().setLayout(null);
		setSize(800,500);
		setLocationRelativeTo(null);
		setVisible(false);
	}
	
	private void addComponents(){
		
		getContentPane().add(ch_img);
		
		getContentPane().add(facImage);
		getContentPane().add(fNLbl);
		getContentPane().add(mNLbl);
		getContentPane().add(lNLbl);
		getContentPane().add(adLbl);
		getContentPane().add(bdLbl);
		getContentPane().add(ageLbl);
		getContentPane().add(conLbl);
		getContentPane().add(eLbl);
		getContentPane().add(degLbl);
		getContentPane().add(posLbl);
		getContentPane().add(statLbl);
		
		getContentPane().add(fName);
		getContentPane().add(mName);
		getContentPane().add(lName);
		getContentPane().add(Address);
		getContentPane().add(bdate);
		getContentPane().add(age);
		getContentPane().add(contactOne);
		getContentPane().add(contactTwo);
		getContentPane().add(email);
		getContentPane().add(degree);
		getContentPane().add(position);
		getContentPane().add(status);
		
		getContentPane().add(Edit);
		getContentPane().add(Delete);
		getContentPane().add(Save);
		getContentPane().add(Cancel);
		
		getContentPane().add(tPane);
	}
	
	private void initComponents(){
		
		//Labels
		facImage = new JLabel();
		facImage.setIcon(img);
		facImage.setBounds(15, 10, 100, 100);
		
		fNLbl = new  JLabel("First Name:");
		fNLbl.setBounds(125, 10, 80, 20);
		
		mNLbl = new  JLabel("Middle Name:");
		mNLbl.setBounds(125, 50, 80, 20);
		
		lNLbl = new  JLabel("Last Name:");
		lNLbl.setBounds(125,90,80,20);
		
		adLbl = new  JLabel("Address:");
		adLbl.setBounds(15,130,80,20);
		
		bdLbl = new  JLabel("Birthdate:");
		bdLbl.setBounds(15, 170	, 80, 20);
		
		ageLbl = new  JLabel("Age:");
		ageLbl.setBounds(231, 170, 80, 20);
		
		conLbl = new JLabel("Contact #:");
		conLbl.setBounds(15,210,80,20);
		
		eLbl = new JLabel("Email");
		eLbl.setBounds(15,250,80,20);
		
		degLbl = new JLabel("Degree:");
		degLbl.setBounds(15,290,80,20);
		
		posLbl = new JLabel("Position:");
		posLbl.setBounds(15,330,80,20);
		
		statLbl = new JLabel("Status:");
		statLbl.setBounds(15,370,80,20);
		
		
		//TextFields
		fName = new JTextField();
		fName.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		fName.setBounds(210,8,170,26);
		fName.setEditable(false);
		
		mName = new JTextField();
		mName.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		mName.setBounds(210,48,170,26);
		mName.setEditable(false);

		lName = new JTextField();
		lName.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		lName.setBounds(210,88,170,26);
		lName.setEditable(false);
		
		Address = new JTextField();
		Address.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		Address.setBounds(95,128,285,26);
		Address.setEditable(false);
		
		bdate = new JDateChooser();
		bdate.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		bdate.setDateFormatString("MMM dd, yyyy");
		bdate.setBounds(95,168,125,26); 
		bdate.setEnabled(false);
		
		age = new JTextField();
		age.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		age.setBounds(265,168,115,26);
		age.setEditable(false);
		
		contactOne = new JTextField();
		contactOne.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		contactOne.setBounds(95,208,135,26);
		contactOne.setEditable(false);
		
		contactTwo = new JTextField();
		contactTwo.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		contactTwo.setBounds(245,208,135,26);
		contactTwo.setEditable(false);
		
		email = new JTextField();
		email.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		email.setBounds(95,248,285,26);
		email.setEditable(false);
		
		degree = new JTextField();
		degree.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		degree.setBounds(95,288,285,26);
		degree.setEditable(false);
		
		position = new JTextField();
		position.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		position.setBounds(95,328,285,26);
		position.setEditable(false);
		
		status = new JComboBox<String>();
		status.setModel(new DefaultComboBoxModel<String>(new String[] {"Active","On Leave","Inactive"}));
		status.setEditable(false);
		status.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		status.setBounds(95,368,285,26);
		status.setEnabled(false);
		
		//Buttons
		ch_img = new JButton();
		ch_img.setOpaque(false);
		ch_img.setBorderPainted(false);
		ch_img.setContentAreaFilled(false);
		ch_img.setRolloverIcon(new ImageIcon("img/ch_photo.png"));
		ch_img.setDisabledIcon(null);
		ch_img.setBounds(15, 10, 100, 100);
		ch_img.setEnabled(false);
		
		Edit = new JButton("EDIT");
		Edit.setBounds(95,408,80,45);
		
		Delete = new JButton("DELETE");
		Delete.setBounds(225,408,80,45);
		
		Save = new JButton("SAVE");
		Save.setBounds(95,408,80,45);
		Save.setVisible(false);
		
		Cancel = new JButton("CANCEL");
		Cancel.setBounds(225,408,80,45);
		Cancel.setVisible(false);
		
		//TabbedPane
		tPane = new JTabbedPane(JTabbedPane.TOP);
		tPane.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		tPane.setBounds(400,9,375,441);
		
		subjectTab = Subject_Tab.getInstance();
		publicationTab = Publication_Tab.getInstance();
		activityTab = Activity_Tab.getInstance();
		
		tPane.addTab("Subjects", subjectTab);
		tPane.addTab("Publications", publicationTab);
		tPane.addTab("Activities", activityTab);
	}	
	
	public void showProfile(Faculty faculty,int row){
		current_row = row;
		this.faculty = faculty;
		initFields();
		showFields(false);
		updateTabs(faculty.id);
		setVisible(true);
	}
	
	private void updateTabs(int faculty_id) {
		try {
			subjectTab.updateTable(Subject.getSubjects(faculty_id));
		} catch (ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(this, "An error occurred while getting the subjects of the faculty. The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		try {
			publicationTab.updateTable(Publication.getPublications(faculty_id));
		} catch (ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(this, "An error occurred while getting the publications of the faculty. The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		try {
			activityTab.updateTable(Activity.getActivities(faculty_id));
		} catch (ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(this, "An error occurred while getting the activities of the faculty. The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("static-access")
	public void initFields(){
		
		if(!faculty.path.isEmpty()){
			File file = new File(faculty.path);
			try {
					facImage.setIcon(file.exists()? new ImageIcon(Util.getInstance().resizeImage(ImageIO.read(file), 100, 100)) : img);
			} catch (IOException e) {
					e.printStackTrace();
			}
		}
		else
			facImage.setIcon(img);
		
		fName.setText(faculty.fName);
		mName.setText(faculty.mName);
		lName.setText(faculty.lName);
		Address.setText(faculty.addr);
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		try {
			date = formatter.parse(Faculty_List.getInstance().facList.get(current_row).bdate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		bdate.setDate(date);
		age.setText(faculty.age + " years old");
		contactOne.setText(faculty.conOne);
		if(!faculty.conTwo.isEmpty())
			contactTwo.setText(faculty.conTwo);
		email.setText(faculty.email);
		degree.setText(faculty.degree);
		position.setText(faculty.position);
		status.setSelectedIndex(faculty.stat_int);
	}
	
	public void showFields(boolean set){
		
		fName.setEditable(set);
		mName.setEditable(set);
		lName.setEditable(set);
		Address.setEditable(set);
		bdate.setEnabled(set);
		contactOne.setEditable(set);
		contactTwo.setEditable(set);
		email.setEditable(set);
		degree.setEditable(set);
		position.setEditable(set);
		status.setEnabled(set);
		
		Edit.setVisible(!set);
		Delete.setVisible(!set);
		Save.setVisible(set);
		Cancel.setVisible(set);
		ch_img.setEnabled(set);
		
	}
	
	public boolean verifyInput(){
		
		boolean valid = true;
		
		String errMsg = "";
		String firstNme = fName.getText().trim();
		String midNme = mName.getText().trim();
		String lastNme = lName.getText().trim();
		String addr = Address.getText().trim();
		String conOne = contactOne.getText().trim().isEmpty() && !contactTwo.getText().trim().isEmpty()? contactTwo.getText().trim() : contactOne.getText().trim();
		String eAdd = email.getText().trim();
		String deg = degree.getText().trim();
		String pos = position.getText().trim();
		String stat = status.getSelectedItem().toString();
		
		String regx = "^[\\p{L} .'-]+$";
		if(firstNme.isEmpty()||midNme.isEmpty()||lastNme.isEmpty()||addr.isEmpty()||
			conOne.isEmpty()||eAdd.isEmpty()||deg.isEmpty()||pos.isEmpty()||stat.isEmpty()){
				
			errMsg = errMsg + "Please fill up all the fields! 2nd Contact Number is Optional..\n";
			valid = false;
		}
			
		//Names
		if(!firstNme.matches(regx) && !firstNme.isEmpty()){
			
			errMsg = errMsg + "First Name must only contain letters and white space characters!\n";
			valid = false;
		}
			
		if(!midNme.matches(regx) && !midNme.isEmpty()){
			
			errMsg = errMsg + "Middle Name must only contain letters and white space characters!\n";
			valid = false;
		}

		if(!lastNme.matches(regx) && !lastNme.isEmpty()){
			
			errMsg = errMsg + "Last Name must only contain letters, white space characters and a dot!\n";
			valid = false;
		}
			
		//check valid email
		if(!EmailValidator.getInstance().isValid(eAdd) && !eAdd.isEmpty()){
			
			errMsg = errMsg + "Invalid Email Address!\n";
			valid = false;
		}
		
		if(!valid)
			JOptionPane.showMessageDialog(this, errMsg, "Input Errors", JOptionPane.ERROR_MESSAGE);
		
		return valid;
	}
	
	@SuppressWarnings("static-access")
	public void changeImage(){
		file = null;
		String path="";
		try {
			path = Util.getInstance().chooseImage();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		if(!path.isEmpty()){
			
			hasChosen = true;
			file = new File(path);
	
			try {
				facImage.setIcon(new ImageIcon(Util.getInstance().resizeImage(ImageIO.read(file), 100, 100)));
				facImage.setSize(100, 100);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public Faculty getFaculty(){
		return faculty;
	}

	public static Faculty_Profile getInstance() {
		if(instance == null)
			instance = new Faculty_Profile();
		return instance;
	}
}
