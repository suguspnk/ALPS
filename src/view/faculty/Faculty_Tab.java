package view.faculty;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.font.TextAttribute;
import java.util.ArrayList;
import java.util.Map;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import model.faculty.Faculty;

public class Faculty_Tab extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8807459329250379227L;

	private static Faculty_Tab facultyTab;
	
	public JTextField search;
	public JButton searchBtn, addBtn,prev,next;
	public JTable tbl;
	
	@SuppressWarnings("rawtypes")
	public JComboBox options,pages;
	public DefaultComboBoxModel<String> pageModel;
	public JScrollPane scrollPane;
	public DefaultTableModel tblModel = new DefaultTableModel(0, 0);
	public TableColumnModel colModel;
	private static final String columnNames[] = { "Last Name", "First Name", "Middle Name", "Birthdate", "Address",
		"Email","Contact #","Action"};
	
	public int numPages = 0;
	
    public Faculty_Tab(){
		
		super();
		
		initComponents();
		addComponents();
		
		setPanelLayout();
		
	}
	
	private void setPanelLayout(){
		setLayout(null);
		setSize(585,485);
		setVisible(false);
	}
	
	private void addComponents(){
		
		add(next);
		add(prev);
		add(pages);
		add(options);
		add(search);
		add(searchBtn);
		add(addBtn);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initComponents(){
		
		options = new JComboBox();
		options.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 13));
		options.setModel(new DefaultComboBoxModel(new String[] { "All", "Last Name","First Name","Middle Name","Birthdate",
				"Address","Email","Contact #"}));
		options.setEditable(false);
		options.setBounds(10,21,91,30);
		
		search = new JTextField();
		search.setBounds(102,21,300,30);
		
		searchBtn = new JButton("Search");
		searchBtn.setFont(new Font("Segoe UI Semulight",Font.PLAIN,13));
		searchBtn.setBounds(402,21,84,30);
		
		addBtn = new JButton("Add");
		addBtn.setFont(new Font("Segoe UI Semilight",Font.PLAIN, 13));
		addBtn.setBounds(486,21,84,30);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10,63,560,325);
		add(scrollPane);
		
		tbl = new JTable(){
	    	 private static final long serialVersionUID = 1L;

	         public boolean isCellEditable(int row, int column) {                
	                 return false;               
	         };
	         
	    };
	    
	    tblModel.setColumnIdentifiers(columnNames);
	    tbl.setModel(tblModel);
		tbl.setFillsViewportHeight(true);
		tbl.getTableHeader().setReorderingAllowed(false);
	    tbl.getTableHeader();
	    tbl.setSelectionBackground(Color.DARK_GRAY);
	    tbl.setSelectionForeground(Color.WHITE);
	    tbl.getColumn("Action").setMaxWidth(50);
	    tbl.getColumn("Action").setMinWidth(50);
	    tbl.setSize(560, 325);
	    colModel = tbl.getColumnModel();
		for(int j = 0; j < colModel.getColumnCount(); j++)
			colModel.getColumn(j).setCellRenderer(new RowRenderer());
	    scrollPane.setViewportView(tbl);
		
	    pageModel = new DefaultComboBoxModel<String>(new Vector<String>());
	    pages = new JComboBox();
	    pages.setBounds(266, 390, 77, 24);
	    
	    prev = new JButton("Prev");
		prev.setBounds(11, 390, 80, 25);
		prev.setEnabled(false);
		
		next = new JButton("Next");
		next.setBounds(490, 390, 80, 25);
		next.setEnabled(false);
		
	    JLabel lblPage = new JLabel("Page:");
		lblPage.setBounds(226, 395, 55, 15);
		add(lblPage);
		setVisible(false);
	}
	
	public void showPanel(){
		setVisible(true);
	}
	
	public void update(ArrayList<Faculty> facList){
		
		tblModel.setRowCount(0);
		
		
		for(int i=0;i<facList.size();i++){
			String contact = facList.get(i).conOne + (!facList.get(i).conTwo.isEmpty()? ", " + facList.get(i).conTwo: "");
			tblModel.addRow(new Object[]{facList.get(i).lName,facList.get(i).fName,facList.get(i).mName,facList.get(i).bdate,
					facList.get(i).addr, facList.get(i).email, contact, "View"});
		}
	}
	
	public static Faculty_Tab getInstance(){
		
		if(facultyTab == null)
			facultyTab = new Faculty_Tab();
		return facultyTab;
	}
	
	@SuppressWarnings("serial")
	class RowRenderer extends DefaultTableCellRenderer {
	    @SuppressWarnings({ "rawtypes", "unchecked" })
		public Component getTableCellRendererComponent(JTable table,
	                                                   Object value,
	                                                   boolean isSelected,
	                                                   boolean hasFocus,
	                                                   int row, int column) {
	        super.getTableCellRendererComponent(table, value, isSelected,
	                                            hasFocus, row, column);
	        if(column!=7){
	        	String tip = (String)table.getModel().getValueAt(row, column);
	        	setToolTipText(tip);
	        }
	        
	        if (column == 7) {
	        	if(!isSelected)
	        		this.setForeground(new Color(51, 102, 153));
	        	else
	        		this.setForeground(new Color(204, 255, 255));
			    Font font = this.getFont();
			    Map attributes = font.getAttributes();
			    attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
			    this.setFont(font.deriveFont(attributes));
			    this.setHorizontalAlignment(SwingConstants.CENTER);
			}
	        return this;
	    }
	}
	
	@SuppressWarnings("unchecked")
	public void updatePages(int numPages){
		
		this.numPages = numPages;
		String n_pages[] = new String[numPages];
		for(int i=0;i<numPages;i++)
			n_pages[i] = "" + (i+1);
		pageModel = new DefaultComboBoxModel<String>(n_pages);
		pages.setModel(pageModel);
		next.setEnabled(numPages>1? true:false);
		
	}

}
