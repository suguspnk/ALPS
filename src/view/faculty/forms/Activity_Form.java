package view.faculty.forms;

import java.awt.Dimension;
import java.awt.Font;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;










import view.Form;

import com.toedter.calendar.JDateChooser;

import javax.swing.JButton;

import model.faculty.entities.Activity;

@SuppressWarnings("serial")
public class Activity_Form extends Form {
	
	private static Activity_Form instance;
	public JLabel title;
	public JTextField titleField;
	public JLabel date;
	public JLabel venue;
	public JTextField venueField;
	public JLabel type;
	public JRadioButton universityService;
	public JRadioButton communityService;
	private JDateChooser dateField;
	public JButton btnUpdate;
	
	public Activity_Form() {
		super();
		initComponents();
		addComponents();
		setFormLayout();
	}
	
	public void setFormLayout(){
		setResizable(false);
		setSize(new Dimension(400, 300));
		setLocationRelativeTo(null);
		setTitle("Activity");
		setVisible(false);
	}
	
	private void initComponents(){
		
		Font textFont = Subject_Form.textFont;
		Font fieldFont = Subject_Form.fieldFont;
		
		title = new JLabel("Title of Activity:");
		title.setFont(textFont);
		title.setBounds(15, 10, 120, 30);
		
		titleField = new JTextField(10);
		titleField.setFont(fieldFont);
		titleField.setBounds(140, 12, 240, 30);
		
		date = new JLabel("Date:");
		date.setFont(textFont);
		date.setBounds(15, 50, 50, 30);
		
		venue = new JLabel("Venue:");
		venue.setFont(textFont);
		venue.setBounds(15, 90, 60, 30);
		
		venueField = new JTextField(10);
		venueField.setFont(fieldFont);
		venueField.setBounds(70, 90, 310, 30);
		
		type = new JLabel("Type of Activity:");
		type.setFont(textFont);
		type.setBounds(15, 130, 120, 30);
		
		universityService = new JRadioButton("University Service");
		universityService.setFont(textFont);
		universityService.setBounds(30, 160, 150, 30);
		universityService.setSelected(true);
		
		communityService = new JRadioButton("Community Service");
		communityService.setFont(textFont);
		communityService.setBounds(200, 160, 160, 30);
		
		ButtonGroup group = new ButtonGroup();
		group.add(universityService);
		group.add(communityService);
		
		btnUpdate = new JButton("Update");

		btnUpdate.setFont(textFont);
		btnAdd.setFont(textFont);
		btnCancel.setFont(textFont);
		
		btnAdd.setBounds(120, 230, 80, 30);
		btnUpdate.setBounds(120, 230, 80, 30);
		btnCancel.setBounds(210, 230, 80, 30);
		
		dateField = new JDateChooser();
		dateField.setDateFormatString("MMM dd, yyyy");
		dateField.setBounds(70, 51, 167, 29);
	}

	public void addComponents(){
		getContentPane().add(title);
		getContentPane().add(titleField);
		getContentPane().add(date);
		getContentPane().add(dateField);
		getContentPane().add(venue);
		getContentPane().add(venueField);
		getContentPane().add(type);
		getContentPane().add(universityService);
		getContentPane().add(communityService);
		getContentPane().add(btnAdd);
		getContentPane().add(btnUpdate);
		getContentPane().add(btnCancel);
	}
	
	/**
	 * i represents what to show, whether an add form or an update form
	 * @param i - if i = 0 then add else edit
	 */
	public void showActivityForm(int i, Activity activity){
		if(activity == null)
			clear();
		else
			setFields(activity);
		btnAdd.setVisible(i==0);
		btnUpdate.setVisible(i==1);
		setVisible(true);
	}
	
	private void setFields(Activity activity) {
		titleField.setText(activity.title);
		DateFormat format= new SimpleDateFormat("yyyy-MM-dd");
		try {
			dateField.setDate(format.parse(activity.date));
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(this, "An error occurred while parsing the date. The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			return;
		}
		venueField.setText(activity.venue);
		universityService.setSelected(activity.type == 0);
		communityService.setSelected(activity.type == 1);
	}

	@Override
	public boolean verifyInput() {
		String titleText = titleField.getText().trim();
		String venueText = venueField.getText().trim();
		
		if(titleText.isEmpty() || venueText.isEmpty() ){
			JOptionPane.showMessageDialog(this, "The title and venue should not be empty!", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		try{
			Date date = dateField.getDate();
			if(date == null){
				JOptionPane.showMessageDialog(this, "The date is invalid!", "Date Error", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}catch(Exception e){
			JOptionPane.showMessageDialog(this, "The date is invalid!", "Date Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	public int getActivityType() {
		if(universityService.isSelected())
			return 0;
		else	// if community service
			return 1;
	}

	public String getActivityTitle() {
		return titleField.getText().trim();
	}

	public String getVenue() {
		return venueField.getText().trim();
	}

	public Date getDate() {
		return dateField.getDate();
	}

	public void clear() {
		titleField.setText("");
		dateField.setDate(new Date());
		venueField.setText("");
		universityService.setSelected(true);
	}

	public static Activity_Form getInstance() {
		if(instance == null)
			instance = new Activity_Form();
		return instance;
	}
}
