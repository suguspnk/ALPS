package view.faculty.forms;

import java.awt.Dimension;
import java.awt.Font;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.faculty.entities.Publication;
import view.Form;

import com.toedter.calendar.JDateChooser;

@SuppressWarnings("serial")
public class Publication_Form extends Form {
	
	public JLabel title;
	public JTextField titleField;
	public JLabel date;
	public JLabel type;
	public JComboBox<String> typePublic;
	public JLabel upload;
	public JTextField uploadField;
	public JButton browse;
	public JButton btnUpdate;
	private JDateChooser dateField;
	public static String publication_types[] = new String [] {"Thesis", "Journal", "Scientific Paper", "Research Paper"};
	private static Publication_Form instance;
	
	public Publication_Form() {
		super();
		initComponents();
		addComponents();
		setFormLayout();
	}
	
	public void setFormLayout(){
		setResizable(false);
		setSize(new Dimension(400,300));
		setLocationRelativeTo(null);
		setTitle("Publication");
		setVisible(false);
	}
	
	private void initComponents(){
		
		Font textFont = Subject_Form.textFont;
		Font fieldFont = Subject_Form.fieldFont;
		
		title = new JLabel("Title:");
		title.setFont(textFont);
		title.setBounds(15, 10, 50, 30);
		
		titleField = new JTextField(10);
		titleField.setFont(fieldFont);
		titleField.setBounds(60, 12, 320, 30);
		
		date = new JLabel("Date Published:");
		date.setFont(textFont);
		date.setBounds(15, 50, 140, 30);
		
		type = new JLabel("Type of Publication:");
		type.setFont(textFont);
		type.setBounds(15, 90, 140, 30);
		
		typePublic = new JComboBox<String>();
		typePublic.setModel(new DefaultComboBoxModel<String>(publication_types));
		typePublic.setEditable(false);
		typePublic.setFont(textFont);
		typePublic.setBounds(160, 90, 220, 30);
		
		upload = new JLabel("Upload File:");
		upload.setFont(textFont);
		upload.setBounds(15, 130, 120, 30);
		
		uploadField = new JTextField(10);
		uploadField.setFont(fieldFont);
		uploadField.setBounds(15, 160, 280, 30);
		
		browse = new JButton("browse");
		browse.setFont(textFont);
		browse.setBounds(300, 160, 80, 30);
		
		btnUpdate = new JButton("Update");
		btnUpdate.setBounds(81, 225, 90, 28);
		
		btnAdd.setFont(textFont);
		btnUpdate.setFont(textFont);
		btnCancel.setFont(textFont);
		
		btnAdd.setBounds(120, 230, 80, 30);
		btnUpdate.setBounds(120, 230, 80, 30);
		btnCancel.setBounds(210, 230, 80, 30);
		
		dateField = new JDateChooser();
		dateField.setDateFormatString("MMM dd, yyyy");
		dateField.setBounds(130, 50, 250, 28);
	}
	
	private void addComponents(){
		getContentPane().add(title);
		getContentPane().add(titleField);
		getContentPane().add(date);
		getContentPane().add(type);
		getContentPane().add(typePublic);
		getContentPane().add(upload);
		getContentPane().add(uploadField);
		getContentPane().add(browse);
		getContentPane().add(btnAdd);
		getContentPane().add(btnUpdate);
		getContentPane().add(btnCancel);
		getContentPane().add(dateField);
	}

	/**
	 * i represents what to show, whether an add form or an update form
	 * @param i - if i = 0 then add else edit
	 */
	public void showPublicationForm(int i, Publication publication){
		if(publication == null)
			clear();
		else
			setFields(publication);
		btnAdd.setVisible(i==0);
		btnUpdate.setVisible(i==1);
		setVisible(true);
	}
	
	public void clear() {
		titleField.setText("");
		dateField.setDate(new Date());
		uploadField.setText("");
		typePublic.setSelectedIndex(0);
	}
	
	private void setFields(Publication publication) {
		titleField.setText(publication.title);
		DateFormat format= new SimpleDateFormat("yyyy-MM-dd");
		try {
			dateField.setDate(format.parse(publication.date_published));
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(this, "An error occurred while parsing the date. The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			return;
		}
		uploadField.setText(publication.filepath);
		typePublic.setSelectedIndex(publication.type);
	}

	@Override
	public boolean verifyInput() {
		String titleText = titleField.getText().trim();
		String filePath = uploadField.getText().trim();
		
		if(titleText.isEmpty() || filePath.isEmpty() ){
			JOptionPane.showMessageDialog(this, "The title and filepath should not be empty!", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if(!new File(filePath).exists()){
			JOptionPane.showMessageDialog(this, "The filepath does not exist!", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		try{
			Date date = dateField.getDate();
			if(date == null){
				JOptionPane.showMessageDialog(this, "The date is invalid!", "Date Error", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}catch(Exception e){
			JOptionPane.showMessageDialog(this, "The date is invalid!", "Date Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	public int getPublicationType() {
		return typePublic.getSelectedIndex();
	}

	public String getPublicationTitle() {
		return titleField.getText().trim();
	}

	public Date getDate_Published() {
		return dateField.getDate();
	}

	public String getFilePath() {
		return uploadField.getText().trim();
	}

	public static Publication_Form getInstance() {
		if(instance == null)
			instance = new Publication_Form();
		return instance;
	}
}
