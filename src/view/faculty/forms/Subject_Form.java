package view.faculty.forms;

import java.awt.Dimension;
import java.awt.Font;
import java.io.File;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import model.faculty.entities.Subject;
import view.Form;

@SuppressWarnings("serial")
public class Subject_Form extends Form{

	public static Font fieldFont = new Font("Segoe UI SemiLight", Font.PLAIN, 12);
	public static Font textFont = new Font("Segoe UI SemiBold", Font.PLAIN, 14);
	
	private static Subject_Form instance;
	public JLabel title;
	public JTextField titleField;
	public JLabel time;
	public JLabel room;
	public JTextField roomField;
	public JLabel semester;
	public JRadioButton sem1;
	public JRadioButton sem2;
	public JLabel upload;
	public JTextField uploadField;
	public JButton browse;
	public JTextField timeField;
	public JButton btnUpdate;
	
	public Subject_Form() {
		super();
		initComponents();
		addComponents();
		setFormLayout();
	}
	
	public void setFormLayout(){
		setResizable(false);
		setSize(new Dimension(400, 300));
		setLocationRelativeTo(null);
		setTitle("Subject");
		setVisible(false);
	}
	
	private void initComponents(){
		
		title = new JLabel("Subject Title:");
		title.setFont(textFont);
		title.setBounds(15, 10, 100, 30);
		
		titleField = new JTextField(10);
		titleField.setBounds(120, 12, 260, 30);
		
		time = new JLabel("Time:");
		time.setFont(textFont);
		time.setBounds(15, 50, 50, 30);
		
		timeField = new JTextField(10);
		timeField.setBounds(20, 80, 160, 25);
		
		room = new JLabel("Room:");
		room.setFont(textFont);
		room.setBounds(220, 50, 50, 30);
		
		roomField = new JTextField(10);
		roomField.setBounds(220, 80, 160, 25);
		
		semester = new JLabel("Semester:");
		semester.setFont(textFont);
		semester.setBounds(15, 113, 100, 30);
		
		sem1 = new JRadioButton("1st Semester");
		sem1.setFont(textFont);
		sem1.setBounds(98, 113, 120, 30);
		
		sem2 = new JRadioButton("2nd Semester");
		sem2.setFont(textFont);
		sem2.setBounds(220, 113, 140, 30);
		
		sem1.setSelected(true);
		
		ButtonGroup group = new ButtonGroup();
		group.add(sem1);
		group.add(sem2);
		
		upload = new JLabel("Upload Syllabus:");
		upload.setFont(textFont);
		upload.setBounds(15, 150, 120, 30);
		
		uploadField = new JTextField(10);
		uploadField.setFont(fieldFont);
		uploadField.setBounds(15, 180, 280, 30);
		
		browse = new JButton("Browse");
		browse.setFont(textFont);
		browse.setBounds(300, 180, 80, 30);
		
		btnUpdate = new JButton("Update");

		btnUpdate.setFont(textFont);
		btnAdd.setFont(textFont);
		btnCancel.setFont(textFont);
		
		btnAdd.setBounds(120, 230, 80, 30);
		btnUpdate.setBounds(158, 230, 80, 30);
		btnCancel.setBounds(210, 230, 80, 30);
	}

	public void addComponents(){
		getContentPane().add(title);
		getContentPane().add(titleField);
		getContentPane().add(time);
		getContentPane().add(timeField);
		getContentPane().add(room);
		getContentPane().add(roomField);
		getContentPane().add(semester);
		getContentPane().add(sem1);
		getContentPane().add(sem2);
		getContentPane().add(upload);
		getContentPane().add(uploadField);
		getContentPane().add(browse);
		
		getContentPane().add(btnAdd);
		getContentPane().add(btnUpdate);
		getContentPane().add(btnCancel);
	}
	
	/**
	 * i represents what to show, whether an add form or an update form
	 * @param i - if i = 0 then add else edit
	 */
	public void showSubjectForm(int i, Subject subject){
		if(subject == null)
			clear();
		else
			setFields(subject);
		btnAdd.setVisible(i==0);
		btnUpdate.setVisible(i==1);
		setVisible(true);
	}
	
	private void setFields(Subject subject) {
		titleField.setText(subject.title);
		timeField.setText(subject.time);
		roomField.setText(subject.room);
		uploadField.setText(subject.syllabusFile);
		sem1.setSelected(subject.sem == 0);
		sem2.setSelected(subject.sem == 1);
	}

	@Override
	public boolean verifyInput() {
		String title = titleField.getText().trim();
		String room = roomField.getText().trim();
		String time = timeField.getText().trim();
		String syllabusPath = uploadField.getText().trim();
		
		if(title.isEmpty() || room.isEmpty() || time.isEmpty() || syllabusPath.isEmpty()){
			JOptionPane.showMessageDialog(this, "The input fields should not be empty!", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if(!new File(syllabusPath).exists()){
			JOptionPane.showMessageDialog(this, "The filepath of the syllabus does not exist!", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	public int getSem() {
		if(sem1.isSelected())
			return 0;
		else	// if community service
			return 1;
	}

	public String getSubjectTitle() {
		return titleField.getText().trim();
	}

	public String getRoom() {
		return roomField.getText().trim();
	}

	public String getTime() {
		return timeField.getText().trim();
	}
	
	public String getSyllabus(){
		return uploadField.getText().trim();
	}

	public void clear() {
		titleField.setText("");
		timeField.setText("hh:mm aa to hh:mm aa");
		roomField.setText("");
		uploadField.setText("");
		sem1.setSelected(true);
	}

	public static Subject_Form getInstance() {
		if(instance == null)
			instance = new Subject_Form();
		return instance;
	}
}
