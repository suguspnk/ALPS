package view.faculty.forms;

import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.apache.commons.validator.routines.EmailValidator;

import com.toedter.calendar.JDateChooser;

import others.Util;
import view.Form;

@SuppressWarnings("serial")
public class Faculty_Form extends Form {
	
	private static Faculty_Form instance;
	public JTextField fName,mName,lName,Address,contactOne,contactTwo,email,
	degree,position;
	public JComboBox<String> status;
	public JButton ch_img;
	public JDateChooser bdate;
	public boolean hasChosen = false;
	private JLabel facImage,fNLbl,mNLbl,lNLbl,adLbl,bdLbl,conLbl,eLbl,degLbl,posLbl,statLbl;
	private ImageIcon img = new ImageIcon("img/blankImg.png");
	
	public File file;

	public Faculty_Form() {
		
		super();
		
		initComponents();
		addComponents();
		
		setFormLayout();
	}
	
	public void setFormLayout(){
		setTitle("A.L.P.S - Add Faculty Form");
		setSize(400,480);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(false);
	}
	
	public void initComponents(){
		//Labels
		facImage = new JLabel();
		facImage.setIcon(img);
		facImage.setBounds(15, 10, 100, 100);
		
		fNLbl = new  JLabel("First Name:");
		fNLbl.setBounds(125, 10, 80, 20);
		
		mNLbl = new  JLabel("Middle Name:");
		mNLbl.setBounds(125, 50, 80, 20);
		
		lNLbl = new  JLabel("Last Name:");
		lNLbl.setBounds(125,90,80,20);
		
		adLbl = new  JLabel("Address:");
		adLbl.setBounds(15,130,80,20);
		
		bdLbl = new  JLabel("Birthdate:");
		bdLbl.setBounds(15, 170	, 80, 20);
		
		conLbl = new JLabel("Contact #:");
		conLbl.setBounds(15,210,80,20);
		
		eLbl = new JLabel("Email");
		eLbl.setBounds(15,250,80,20);
		
		degLbl = new JLabel("Degree:");
		degLbl.setBounds(15,290,80,20);
		
		posLbl = new JLabel("Position:");
		posLbl.setBounds(15,330,80,20);
		
		statLbl = new JLabel("Status:");
		statLbl.setBounds(15,370,80,20);
		
		//TextFields
		fName = new JTextField();
		fName.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		fName.setBounds(210,8,170,26);
		
		mName = new JTextField();
		mName.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		mName.setBounds(210,48,170,26);

		lName = new JTextField();
		lName.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		lName.setBounds(210,88,170,26);

		Address = new JTextField();
		Address.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		Address.setBounds(95,128,285,26);
		
		bdate = new JDateChooser();
		bdate.setDateFormatString("MMM dd, yyyy");
		bdate.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		bdate.setBounds(95,168,285,26);
		
		contactOne = new JTextField();
		contactOne.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		contactOne.setBounds(95,208,135,26);
		
		contactTwo = new JTextField();
		contactTwo.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		contactTwo.setBounds(245,208,135,26);
		
		email = new JTextField();
		email.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		email.setBounds(95,248,285,26);
		
		degree = new JTextField();
		degree.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		degree.setBounds(95,288,285,26);
		
		position = new JTextField();
		position.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		position.setBounds(95,328,285,26);
		
		status = new JComboBox<String>();
		status.setModel(new DefaultComboBoxModel<String>(new String[] {"Active","On Leave","Inactive"}));
		status.setEditable(false);
		status.setFont(new Font("Segoe UI Semulight",Font.PLAIN,12));
		status.setBounds(95,368,285,26);
		
		//Buttons
		ch_img = new JButton();
		ch_img.setOpaque(false);
		ch_img.setBorderPainted(false);
		ch_img.setContentAreaFilled(false);
		ch_img.setRolloverIcon(new ImageIcon("img/ch_photo.png"));
		ch_img.setBounds(15, 10, 100, 100);
		
		btnAdd.setBounds(95,408,80,35);
		btnCancel.setBounds(225,408,80,35);
	}
	
	public void addComponents(){
		
		add(ch_img);
		
		add(facImage);
		add(fNLbl);
		add(mNLbl);
		add(lNLbl);
		add(adLbl);
		add(bdLbl);
		add(conLbl);
		add(eLbl);
		add(degLbl);
		add(posLbl);
		add(statLbl);
		
		add(fName);
		add(mName);
		add(lName);
		add(Address);
		add(bdate);
		add(contactOne);
		add(contactTwo);
		add(email);
		add(degree);
		add(position);
		add(status);
		
		add(btnAdd);
		add(btnCancel);
	}
	
	@Override
	public boolean verifyInput() {
		
		boolean valid = true;
		String errMsg = "";
		String firstNme = fName.getText().trim();
		String midNme = mName.getText().trim();
		String lastNme = lName.getText().trim();
		String addr = Address.getText().trim();
		String conOne = contactOne.getText().trim().isEmpty() && !contactTwo.getText().trim().isEmpty()? contactTwo.getText().trim() : contactOne.getText().trim();
		String eAdd = email.getText().trim();
		String deg = degree.getText().trim();
		String pos = position.getText().trim();
		String stat = status.getSelectedItem().toString();
		
		String regx = "^[\\p{L} .'-]+$";
		
		if(firstNme.isEmpty()||midNme.isEmpty()||lastNme.isEmpty()||addr.isEmpty()||
			conOne.isEmpty()||eAdd.isEmpty()||deg.isEmpty()||pos.isEmpty()||stat.isEmpty()){
			
			errMsg = errMsg + "Please fill up all the fields! 2nd Contact Number is Optional..\n";
			valid = false;
		}
		
		//Names
		if(!firstNme.matches(regx) && !firstNme.isEmpty()){
			
			errMsg = errMsg + "First Name must only contain letters and white space characters!\n";
			valid = false;
		}
		
		if(!midNme.matches(regx) && !midNme.isEmpty()){
			
			errMsg = errMsg + "Middle Name must only contain letters and white space characters!\n";
			valid = false;
		}

		if(!lastNme.matches(regx) && !lastNme.isEmpty()){
			
			errMsg = errMsg + "Last Name must only contain letters, white space characters and a dot!\n";
			valid = false;
		}
		
		//check valid email
		if(!EmailValidator.getInstance().isValid(eAdd) && !eAdd.isEmpty()){
			
			errMsg = errMsg + "Invalid Email Address!\n";
			valid = false;
		}
	
		if(!valid)
			JOptionPane.showMessageDialog(this, errMsg, "Input Errors", JOptionPane.ERROR_MESSAGE);
		return valid;
	}
	
	public void showFaculty_Form(){
		facImage.setIcon(img);
		fName.setText("");
		lName.setText("");
		mName.setText("");
		Address.setText("");
		bdate.setDate(new Date());
		contactOne.setText("");
		contactTwo.setText("");
		email.setText("");
		degree.setText("");
		position.setText("");
		status.setSelectedIndex(0);
		setVisible(true);
	}
	

	@SuppressWarnings("static-access")
	public void setImage() {
		file = null;
		String path="";
		try {
			path = Util.getInstance().chooseImage();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		if(!path.isEmpty()){
			hasChosen = true;
			file = new File(path);
	
			try {
				facImage.setIcon(new ImageIcon(Util.getInstance().resizeImage(ImageIO.read(file), 100, 100)));
				facImage.setSize(100, 100);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static Faculty_Form getInstance() {
		if(instance == null)
			instance = new Faculty_Form();
		return instance;
	}
}


