package view.additionalFunctionalities;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import java.awt.Dimension;

import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.JButton;

import controller.Home_Controller;
import others.Util;
import model.Config;
import model.User;

@SuppressWarnings("serial")
public class Create_User_Frame extends JFrame implements ActionListener{
	private JTextField tfUsername;
	private JPasswordField pwPassword;
	private JButton btnCreate;

	public Create_User_Frame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		
		setSize(new Dimension(350, 205+Config.additionalFrameSize));
		setTitle("Create User");
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		
		JLabel lblNoUserFound = new JLabel("No user found in the database.");
		lblNoUserFound.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		lblNoUserFound.setHorizontalAlignment(SwingConstants.CENTER);
		lblNoUserFound.setBounds(0, 10, 350, 15);
		getContentPane().add(lblNoUserFound);
		
		JLabel label = new JLabel("Create a User.");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		label.setBounds(0, 37, 350, 15);
		getContentPane().add(label);
		
		JPanel inputPanel = new JPanel();
		inputPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		inputPanel.setBounds(10, 59, 328, 113);
		getContentPane().add(inputPanel);
		inputPanel.setLayout(null);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setHorizontalAlignment(SwingConstants.LEFT);
		lblUsername.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		lblUsername.setBounds(12, 14, 83, 15);
		inputPanel.add(lblUsername);
		
		tfUsername = new JTextField();
		tfUsername.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		tfUsername.setBounds(100, 8, 216, 30);
		inputPanel.add(tfUsername);
		tfUsername.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setHorizontalAlignment(SwingConstants.LEFT);
		lblPassword.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		lblPassword.setBounds(12, 50, 83, 15);
		inputPanel.add(lblPassword);
		
		pwPassword = new JPasswordField();
		pwPassword.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		pwPassword.setColumns(10);
		pwPassword.setBounds(100, 44, 216, 30);
		inputPanel.add(pwPassword);
		
		btnCreate = new JButton("Create");
		btnCreate.setFont(new Font("Comic Sans MS", Font.PLAIN, 18));
		btnCreate.setBounds(114, 77, 100, 30);
		inputPanel.add(btnCreate);
		
		btnCreate.addActionListener(this);
		tfUsername.addActionListener(this);
		pwPassword.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == tfUsername)
			tfUsername.transferFocus();
		else{
			String username = tfUsername.getText().trim();
			boolean valid = checkIfUsernameValid(username);
			if(!valid)
				return;
			try {
				User.createUser(username, Util.toString(pwPassword.getPassword()));
				JOptionPane.showMessageDialog(this, 
						"Successfully created the user!", "Congratulations", JOptionPane.INFORMATION_MESSAGE);
				setVisible(false);
				Home_Controller.login.setVisible(true);
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(this, "An error occured when creating the user. The error message is: "+e1.getMessage(), "Create User Error", JOptionPane.ERROR_MESSAGE);
				e1.printStackTrace();
			}
		}
	}

	private boolean checkIfUsernameValid(String username) {
		for(int i = 0; i < username.length(); i++){
			if(Character.isLetterOrDigit(username.charAt(i)) || username.charAt(i) == '_')
				continue;
			JOptionPane.showMessageDialog(this, 
					"Username should contain only letters, numbers and '_'", "Invalid Username", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		try {
			if(User.exists(username)){
				JOptionPane.showMessageDialog(this, 
						"Username already exists. Another computer must be using the same application and created a new user.", "Username Exists", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		} catch (ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(this, 
					"An error occured when checking if the username already exists!", "MySQL Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
