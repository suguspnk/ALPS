package view.additionalFunctionalities;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import controller.Home_Controller;

@SuppressWarnings("serial")
public class Help extends JDialog{
	
	private static Help instance;
	private JScrollPane scrollPane;
	private JTextPane txtpnThisApplicationIs;

	public Help() {
		super(Home_Controller.homeFrame, false);
		initGUI();
	}
	
	private void initGUI() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(new Dimension(300, 300));
		setLocationRelativeTo(Home_Controller.homeFrame);
		setResizable(false);
		setTitle("Help");
		
		scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		txtpnThisApplicationIs = new JTextPane();
		txtpnThisApplicationIs.setContentType("text/html");
		txtpnThisApplicationIs.setEditable(false);
		txtpnThisApplicationIs.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		txtpnThisApplicationIs.setText("<html><body><p font=\"Comic Sans MS\">Explore!!!<br><br>Explore the world!!!<br><br>Explore the application!!!</p></body></html>");
		scrollPane.setViewportView(txtpnThisApplicationIs);
	}
	
	public static Help getInstance(){
		if(instance == null)
			instance = new Help();
		return instance;
	}
}
