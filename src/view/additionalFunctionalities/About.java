package view.additionalFunctionalities;

import javax.swing.JDialog;

import java.awt.Dimension;

import javax.swing.JScrollPane;

import java.awt.BorderLayout;

import javax.swing.JTextPane;

import controller.Home_Controller;

import java.awt.Font;

@SuppressWarnings("serial")
public class About extends JDialog{
	public JScrollPane scrollPane;
	public JTextPane txtpnThisApplicationIs;
	private static About instance;

	public About() {
		super(Home_Controller.homeFrame, false);
		initGUI();
	}
	
	private void initGUI() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(new Dimension(300, 300));
		setLocationRelativeTo(Home_Controller.homeFrame);
		setResizable(false);
		setTitle("About");
		
		scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		txtpnThisApplicationIs = new JTextPane();
		txtpnThisApplicationIs.setEditable(false);
		txtpnThisApplicationIs.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		txtpnThisApplicationIs.setText("This application is a requirement in CMSC 152 with Sir Gilbert Campo.\r\n\r\nMembers:\r\n\tAmasa, Donato\r\n\tCordero, Kenver\r\n\tOcfemia, Danen\r\n\tTelimban, Antonio Jr.\r\n\tVi\u00F1as, Vina");
		scrollPane.setViewportView(txtpnThisApplicationIs);
	}
	
	public static About getInstance(){
		if(instance == null)
			instance = new About();
		return instance;
	}
}
