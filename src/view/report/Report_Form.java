package view.report;

import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JRadioButton;



import view.Form;

@SuppressWarnings("serial")
public class Report_Form extends Form {

	private JLabel from, to, typeOfReport, chooseFaculty;
	
	public JComboBox<String> fmm, fdd, fyy, tmm, tdd, tyy;
	public JComboBox<Object> faculty;
	
	public JRadioButton generalRbtn, specificRbtn;
		
	public Report_Form() {
		super();
		this.setSize(400,430);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		initGUI();
		
	}
	
	@SuppressWarnings("rawtypes")
	public void updateFaculty(Vector data){
		Object[] str = data.toArray();
		ComboBoxModel<Object> model = new DefaultComboBoxModel<Object>(str);
    	faculty.setModel( model );	
		
	}
	
	private void initGUI(){
		
		from = new JLabel("FROM:");
		to = new JLabel("TO:");
		typeOfReport = new JLabel("TYPE OF REPORT:");
		chooseFaculty = new JLabel("CHOOSE FACULTY:");
		
		fmm = new JComboBox<String>();
		fdd = new JComboBox<String>();
		fyy = new JComboBox<String>();
		
		tmm = new JComboBox<String>();
		tdd = new JComboBox<String>();
		tyy = new JComboBox<String>();
		
		add(from);
		from.setBounds(50, 30, 70, 30);
		
		String month[] = new String[] { "(mm)", "January", "Febuary", "March", "April", "May", "June", "Julu", "August", "September", "October",  "November", "December" };
		fmm.setModel(new javax.swing.DefaultComboBoxModel<String>(month));
		add(fmm);
		fmm.setBounds(100, 30, 100, 30);
		
		String day[] = new String[] { "(dd)", "1", "2", "3", "4", "5", "6" ,"7" ,"8" ,"9" ,"10" ,"11" ,"12" ,"13" ,"14" ,"15" ,"16" ,"17" ,"18" ,"19" ,"20" ,"21" ,"22" ,"23" ,"24" ,"25" ,"26" ,"27" ,"28" ,"29" ,"30"};
		fdd.setModel(new javax.swing.DefaultComboBoxModel<String>(day));
		add(fdd);
		fdd.setBounds(220, 30, 70, 30);
		
		String year[] = new String[] { "(yyyy)", "2001", "2002", "2003", "2004", "2005", "2006" ,"2007" ,"2008" ,"2009" ,"2010" ,"2011" ,"2012" ,"2013" ,"2014" ,"2015" ,"2016" ,"2017" ,"2018" ,"2019" ,"2020" ,"2021" ,"2022" ,"2023" ,"2024" ,"2025"};
		add(fyy);
		fyy.setModel(new javax.swing.DefaultComboBoxModel<String>(year));
		fyy.setBounds(310, 30, 80, 30);
		
		add(to);
		to.setBounds(50, 80, 70, 30);
		
		tmm.setModel(new javax.swing.DefaultComboBoxModel<String>(month));
		add(tmm);
		tmm.setBounds(100, 80, 100, 30);
		
		tdd.setModel(new javax.swing.DefaultComboBoxModel<String>(day));
		add(tdd);
		tdd.setBounds(220, 80, 70, 30);
		
		add(tyy);
		tyy.setModel(new javax.swing.DefaultComboBoxModel<String>(year));
		tyy.setBounds(310, 80, 80, 30);
		
		add(typeOfReport);
		typeOfReport.setBounds(30, 125, 300, 40);
		
		generalRbtn = new JRadioButton("General Report");
		add(generalRbtn);
		generalRbtn.setBounds(30, 170, 300, 30);
		specificRbtn = new JRadioButton("Specific Report");
		add(specificRbtn);
		specificRbtn.setBounds(30, 210, 300, 30);
		
		ButtonGroup group = new ButtonGroup();
		group.add(generalRbtn);
		group.add(specificRbtn);
		
		add(chooseFaculty);
		chooseFaculty.setBounds( 140 , 250 ,300, 30);
		
		faculty = new JComboBox<Object>();
		add(faculty);
		faculty.setBounds( 50 , 280 ,300, 30);
		
		add(super.btnAdd);
		btnAdd.setBounds(100, 330, 80, 40);
		
		add(super.btnCancel);
		btnCancel.setBounds(220, 330, 80, 40);
	}
	
	
	public void showPanel(boolean b){
		this.setVisible(b);
	}
	
	public void close(){
		this.setVisible(false);
	}
	
	public JButton getAddButton(){
		return btnAdd;
	}
	@Override
	public boolean verifyInput() {
		if(fmm.getSelectedIndex() == 0 || fdd.getSelectedIndex() == 0 || fyy.getSelectedIndex() == 0 
				|| tmm.getSelectedIndex() == 0 || tdd.getSelectedIndex() == 0 || tyy.getSelectedIndex() == 0)
			return false;
		if(generalRbtn.isSelected() == false && specificRbtn.isSelected() == false)
			return false;
		return true;
	}

}