package view.report;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

import view.Form;

public class Report_Profile extends Form{
	
	JScrollPane scrollPane;
	JTable tbl;
	DefaultTableModel tblModel = new DefaultTableModel();
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JButton generatePDF;
	public JButton deleteReport;
	private JLabel lblPreview;
	
	public Report_Profile(){
		super();
		this.setSize(400,430);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		initGUI();
	}
	
	private BufferedImage renderPDF(String filename){
		File pdfFile = new File(filename);
		RandomAccessFile raf;
		try {
			raf = new RandomAccessFile(pdfFile, "r");
			FileChannel channel = raf.getChannel();
			MappedByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
			PDFFile pdf = new PDFFile(buf);
			PDFPage page = pdf.getPage(0);
			
			Rectangle rect = new Rectangle(0, 0, (int) page.getBBox().getWidth(),
                    (int) page.getBBox().getHeight());
			BufferedImage bufferedImage = new BufferedImage(rect.width, rect.height,
                     BufferedImage.TYPE_INT_RGB);

			java.awt.Image image = page.getImage(rect.width, rect.height,    // width & height
               rect,                       // clip rect
               null,                       // null for the ImageObserver
               true,                       // fill background with white
               true                        // block until drawing is done
			);
			
			Graphics2D bufImageGraphics = bufferedImage.createGraphics();
			bufImageGraphics.drawImage(image, 0, 0, null);
			//				new File("PDF/preview.png").delete();
//				ImageIO.write(bufferedImage, "png", new File( "PDF/preview.png" ));
			raf.close();
			return bufferedImage;
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
		
		// create the image
	}
	
	public void setPreview(String filename){
		ImageIcon preview = new ImageIcon(renderPDF(filename));
		this.lblPreview.setIcon(preview);
	}
	
	public void showDialog(boolean b){
		setVisible(b);
	}
	
	private void initGUI(){
		generatePDF = new JButton("GeneratePDF");
		add(generatePDF);
		generatePDF.setBounds(20, 330, 130, 40);
		
		deleteReport = new JButton("Delete");
		add(deleteReport);
		deleteReport.setBounds(290, 330, 80, 40);
		
		add(super.btnCancel);
		btnCancel.setBounds(180, 330, 80, 40);

		lblPreview = new JLabel();
		scrollPane = new JScrollPane(lblPreview);
		scrollPane.setBounds(20,20,360,300);
		add(scrollPane);
	}

	@Override
	public boolean verifyInput() {
		return false;
	}
	
}
