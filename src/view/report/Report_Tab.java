package view.report;

import java.awt.Font;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class Report_Tab extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8807459329250379227L;
	
	public JTextField search;
	public JButton searchBtn, addBtn;
	public JTable tbl;
	
	@SuppressWarnings("rawtypes")
	public JComboBox options, page;
	public JScrollPane scrollPane;
	public DefaultTableModel tblModel = new DefaultTableModel(0, 0);
	private static final String columnNames[] = { "#", "Type", "Lower Range", "Upper Range", "Action"};
	
	public Report_Tab(){
		
		super();
		
		initComponents();
		addComponents();
		
		setPanelLayout();

	}

	public void updateTable(Object[][] data){
		Object[] str = {"#", "Type", "Lower Range", "Upper Range", "Action"};
		tblModel = new javax.swing.table.DefaultTableModel(data,str);
    	tbl.setModel(tblModel);
    	tbl.getColumnModel().getColumn(0).setMaxWidth(60);
		scrollPane.getViewport().add(tbl,0);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void updatePage(String[] str){
		page.setModel(new DefaultComboBoxModel(str));
	}
	
	private void setPanelLayout(){
		setLayout(null);
		setSize(585,485);
		setVisible(false);
	}
	
	private void addComponents(){
	
		add(page);
		add(options);
		add(search);
		add(searchBtn);
		add(addBtn);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initComponents(){
		
		options = new JComboBox();
		options.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 13));
		options.setModel(new DefaultComboBoxModel(new String[] { "All","Type","Lower Range", "Upper Range"	}));
		options.setEditable(false);
		options.setBounds(20,11,91,30);
		
		search = new JTextField();
		search.setBounds(112,11,220,30);
		
		searchBtn = new JButton("Search");
		searchBtn.setFont(new Font("Segoe UI Semulight",Font.PLAIN,13));
		searchBtn.setBounds(332,11,84,30);
		
		addBtn = new JButton("Add");
		addBtn.setFont(new Font("Segoe UI Semilight",Font.PLAIN, 13));
		addBtn.setBounds(416,11,84,30);
		
		page = new JComboBox();
		page.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 13));
		page.setModel(new DefaultComboBoxModel(new String[] { "1"}));
		page.setEditable(false);
		page.setBounds(500,11,60,30);
		
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(40,53,520,355);
		add(scrollPane);
		
		tbl = new JTable(){
	    	 private static final long serialVersionUID = 1L;

	         public boolean isCellEditable(int row, int column) {                
	                 return false;               
	         };
	    };
	    
	    tblModel.setColumnIdentifiers(columnNames);
	    tbl.setModel(tblModel);
		tbl.setFillsViewportHeight(true);
	    tbl.getTableHeader();
	    scrollPane.setViewportView(tbl);
		
	}
	
	public void showPanel(){
		setVisible(true);
	}
	
}
