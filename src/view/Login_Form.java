package view;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import java.awt.Font;
import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.SwingConstants;

import others.Util;
import model.Config;
import model.SQLConnection;
import model.User;

import javax.swing.border.TitledBorder;
import java.awt.Color;

import javax.swing.border.EtchedBorder;
import javax.swing.JSeparator;

@SuppressWarnings("serial")
public class Login_Form extends JFrame{
	public JTextField usernameTF;
	public JPasswordField passwordPF;
	private JLabel lblPassword;
	public JButton btnLogin;

	public Login_Form() {
		setTitle(Config.title);
		initGUI();
	}

	private void initGUI() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setSize(new Dimension(800, 500+Config.additionalFrameSize));
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel(Config.titleMeaning);
		lblNewLabel_1.setFont(new Font("Segoe UI Semibold", Font.BOLD | Font.ITALIC, 23));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(10, 321, 473, 26);
		getContentPane().add(lblNewLabel_1);
		
		Font font = new Font("Segoe UI", Font.BOLD, 15);
		
		usernameTF = new JTextField();
		usernameTF.setFont(font);
		usernameTF.setHorizontalAlignment(SwingConstants.CENTER);
		usernameTF.setBounds(535, 153, 195, 30);
		getContentPane().add(usernameTF);
		usernameTF.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Username");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		lblNewLabel_2.setBounds(592, 125, 89, 23);
		getContentPane().add(lblNewLabel_2);
		
		passwordPF = new JPasswordField();
		passwordPF.setFont(font);
		passwordPF.setHorizontalAlignment(SwingConstants.CENTER);
		passwordPF.setBounds(535, 245, 195, 26);
		getContentPane().add(passwordPF);
		
		btnLogin = new JButton("login");
		btnLogin.setBackground(Color.WHITE);
		btnLogin.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		btnLogin.setBounds(616, 300, 114, 33);
		getContentPane().add(btnLogin);
		
		JLabel lblNewLabel = new JLabel(Config.title);
		lblNewLabel.setBounds(10, 218, 473, 100);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setPreferredSize(new Dimension(300, 100));
		lblNewLabel.setFont(new Font("Haettenschweiler", Font.PLAIN, 85));
		getContentPane().add(lblNewLabel);
		
		lblPassword = new JLabel("Password");
		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
		lblPassword.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		lblPassword.setBounds(592, 218, 89, 23);
		getContentPane().add(lblPassword);
		
		JLabel lblSdfsd = new JLabel("");
		lblSdfsd.setFont(new Font("Segoe UI Emoji", Font.BOLD, 32));
		lblSdfsd.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Please Login", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		lblSdfsd.setBounds(503, 85, 268, 291);
		getContentPane().add(lblSdfsd);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(20, 313, 463, 50);
		getContentPane().add(separator);
		
		JLabel lblNewLabel_3 = new JLabel(new ImageIcon(Login_Form.class.getResource("/logo/up_logo.png")));
		lblNewLabel_3.setBounds(135, 85, 213, 145);
		getContentPane().add(lblNewLabel_3);
		
		JLabel lblCopyright = new JLabel("Copyright \u00A9 2015");
		lblCopyright.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		lblCopyright.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCopyright.setBounds(683, 449, 102, 14);
		getContentPane().add(lblCopyright);
		
		setLocationRelativeTo(null);
	}
	
	/**
	 * check if the user exists in the database, if the user exists, the resultset is returned if not the null.
	 * @param sqlConnect
	 * @return the resultset of the user found
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public User authenticate(SQLConnection sqlConnect) throws ClassNotFoundException, SQLException{
		String username = usernameTF.getText().trim();
		String password = Util.toString(passwordPF.getPassword());
		ResultSet rs = sqlConnect.query("select * from user where username = '"+SQLConnection.insertBackSlash(username)+"' and password = md5('"+SQLConnection.insertBackSlash(password)+"')");
		try{
			if(rs.next()){	// if there is no username found this statement will throw an exception
				User user = new User();
				user.setId(rs.getString("id"));
				user.setUsername(username);
				user.setPassword(password);
				return user;
			}
		}catch(Exception e){}
		return null;	// return null because no username was found
	}

	public void showLogin() {
		passwordPF.setText("");
		setVisible(true);
	}
}