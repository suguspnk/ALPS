package view.comm;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import model.SQLConnection;
import model.User;
import model.comm.Communication;
import view.Form;

import java.awt.Font;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings({ "serial", "unused" })
public class Comm_Form extends Form {
	
	public JFileChooser browser;

	public JTextField tfFilePath, tfSender, tfTitle;
	public JRadioButton rdbtnIncoming, rdbtnOutgoing;
	public JButton btnBrowse, btnDelete, btnUpdate;
	
	JLabel lblTitle, lblSender, lblUpload;
	ButtonGroup grp;
	
	public Comm_Form() {
		super();
		initComponents();
		addComponents();
		
		setFormLayout();
	}
	
	private void setFormLayout() {
		setTitle("A.L.P.S - Add Communication");
		setSize(300, 220);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(false);
	}

	private void addComponents() {
		getContentPane().add(lblTitle);
		getContentPane().add(tfTitle);
		getContentPane().add(rdbtnIncoming);
		getContentPane().add(rdbtnOutgoing);
		
		grp.add(rdbtnIncoming);
		grp.add(rdbtnOutgoing);
		
		getContentPane().add(lblSender);
		getContentPane().add(tfSender);
		
		getContentPane().add(lblUpload);
		getContentPane().add(tfFilePath);
		getContentPane().add(btnBrowse);
		
		getContentPane().add(btnAdd);
		getContentPane().add(btnCancel);
		
		getContentPane().add(btnUpdate);
		getContentPane().add(btnDelete);
	}

	private void initComponents() {
		lblTitle = new JLabel("<html><font style = font-family:Segoe UI Semibold size = 3><font color = RED>* </font>Title:</font></html>");
		lblTitle.setBounds(5, 13, 45, 23);
		tfTitle = new JTextField();
		tfTitle.setBounds(45, 13, 239, 24);
		
		rdbtnIncoming = new JRadioButton();
		rdbtnIncoming.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		rdbtnIncoming.setText("Incoming");
		rdbtnIncoming.setBounds(76, 44, 71, 23);
		rdbtnOutgoing = new JRadioButton();
		rdbtnOutgoing.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		rdbtnOutgoing.setText("Outgoing");
		rdbtnOutgoing.setBounds(170, 44, 83, 23);
		rdbtnIncoming.setSelected(true);
		grp = new ButtonGroup();
		
		lblSender = new JLabel("<html><font style = font-family:Segoe UI Semibold size = 3><font color = RED>* </font>Sender/Receiver:</font></html>");
		lblSender.setBounds(5, 72, 120, 23);
		tfSender = new JTextField();
		tfSender.setBounds(107, 74, 177, 24);
		
		lblUpload = new JLabel();
		lblUpload.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		lblUpload.setText("Upload File:");
		lblUpload.setBounds(10, 110, 71, 23);
		tfFilePath = new JTextField();
		tfFilePath.setEditable(false);
		tfFilePath.setBounds(83, 110, 128, 24);
		
		btnBrowse = new JButton();
		btnBrowse.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 12));
		btnBrowse.setText("Browse");
		btnBrowse.setBounds(213, 110, 71, 24);
		
		btnAdd.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 12));
		btnAdd.setText("ADD");
		btnAdd.setBounds(67, 156, 71, 26);
		
		btnCancel.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 12));
		btnCancel.setText("CANCEL");
		btnCancel.setBounds(148, 156, 93, 26);
		
		// Browser
		browser = new  JFileChooser();
		SwingUtilities.updateComponentTreeUI(browser);
		
		btnUpdate=new JButton();
		btnUpdate.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 12));
		btnUpdate.setText("UPDATE");
		btnUpdate.setVisible(false);
		btnUpdate.setBounds(63, 156, 80, 26);
		
		btnDelete=new JButton();
		btnDelete.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 12));
		btnDelete.setText("DELETE");
		btnDelete.setVisible(false);
		btnDelete.setBounds(148, 156, 93, 26);
	}

	@Override
	public boolean verifyInput() {
		return !tfTitle.getText().trim().isEmpty() && !tfSender.getText().trim().isEmpty();
	}
	
	public void setCommunicationValues(Communication comm){
		comm.setTitle(tfTitle.getText().trim());
		comm.setType(rdbtnIncoming.isSelected() ? 0 : 1);
		comm.setSender(tfSender.getText().trim());
		comm.setPath(tfFilePath.getText().trim());
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		comm.setDate(dateFormat.format(date));
	}

	public String chooseFile() {
		int valFile = browser.showOpenDialog(null);
    	if(valFile == JFileChooser.APPROVE_OPTION) {
	    	return browser.getSelectedFile().getAbsolutePath();
    	}
    	return null;
	}

	public void clearFields() {
		tfTitle.setText("");
		rdbtnIncoming.setSelected(true);
		tfSender.setText("");
		tfFilePath.setText("");
	}
	
	public void setValues(String title, String type, String sender, String filePath) {
		tfTitle.setText(title);
		tfSender.setText(sender);
		tfFilePath.setText(filePath);
		
		if (type.equalsIgnoreCase("Incoming"))
			rdbtnIncoming.setSelected(true);
		else
			rdbtnOutgoing.setSelected(true);
	}
}