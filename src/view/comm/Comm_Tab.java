package view.comm;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import model.comm.Comm_List;
import model.comm.Communication;

@SuppressWarnings("serial")
public class Comm_Tab extends JPanel{
	
	public JTextField tfSearch;
	public JButton btnSearch, btnAdd;
	public JTable tblSearch;
	DefaultTableModel model = new DefaultTableModel();
	public DefaultComboBoxModel<String> pageModel;
	
	public int page = 0;
	public static String columns[] = new String[] {
		"Title", "Date", "Type", "Sender/Receiver", "File"
	};
	
	@SuppressWarnings("rawtypes")
	public JComboBox comboBox;
	public JButton btnPrev;
	public JButton btnNext;
	@SuppressWarnings("rawtypes")
	public JComboBox cbPage;
	
	public Comm_Tab() {
		super();
		setSize(new Dimension(585, 450));
		
		initComponents();
		addComponents();
	       
		setPanelLayout();
	}

	private void setPanelLayout() {
		setLayout(null);
		setSize(585, 450);
	}

	private void addComponents() {
		add(comboBox);
		add(tfSearch);
		add(btnSearch);
		add(btnAdd);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initComponents() {
		comboBox = new JComboBox();
		comboBox.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 13));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Show All", "Title", "Type", "Sender/Receiver", "File"}));
		comboBox.setBounds(10, 11, 112, 25);
		
		tfSearch = new JTextField();
		tfSearch.setBounds(132, 11, 260, 25);
		tfSearch.setEnabled(false);
		
		btnSearch = new JButton();
		btnSearch.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 13));
		btnSearch.setText("Search");
		btnSearch.setBounds(400, 11, 84, 25);
		btnSearch.setEnabled(false);
		
		btnAdd = new JButton();
		btnAdd.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 13));
		btnAdd.setText("Add");
		btnAdd.setBounds(496, 11, 77, 25);
		

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 46, 563, 343);
		add(scrollPane);
		
		tblSearch = new JTable(){
			public String getToolTipText(MouseEvent e) {
                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);

                try {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } catch (RuntimeException e1) {
                    //catch null pointer exception if mouse is over an empty line
                }
                return tip;
            }
			
			DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

		    { // initializer block
		        renderRight.setHorizontalAlignment(SwingConstants.CENTER);
		    }

		    @Override
		    public TableCellRenderer getCellRenderer (int arg0, int arg1) {
		    	renderRight.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		        return renderRight;
		    }
		};
		
		tblSearch.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				if(tblSearch.columnAtPoint(e.getPoint()) == 0){
//					tblSearch.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)																																																																																																																																						);
				}
				else{																																									
//					tblSearch.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
			}
		});

		tblSearch.setRowHeight(21);
		tblSearch.setModel(new DefaultTableModel(
			new Object[][] {},
			columns
		));
		tblSearch.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		
		tblSearch.setEnabled(false);
		
		scrollPane.setViewportView(tblSearch);
		
		btnPrev = new JButton("Prev");
		btnPrev.setBounds(20, 390, 80, 25);
		btnPrev.setEnabled(false);
		add(btnPrev);
		
		btnNext = new JButton("Next");
		btnNext.setBounds(481, 390, 80, 25);
		btnNext.setEnabled(false);
		add(btnNext);
		
		pageModel = new DefaultComboBoxModel<String>(new Vector<String>());
		cbPage = new JComboBox(pageModel);
		cbPage.setBounds(266, 390, 77, 24);
		add(cbPage);
		
		JLabel lblPage = new JLabel("Page:");
		lblPage.setBounds(216, 395, 55, 15);
		add(lblPage);
		setVisible(false);
	}
	
	public void showPanel() {
		this.setVisible(true);
	}

	public static int numPages, tmpNumPages;
	
	public void updateGUI(Comm_List comm_list) {
		updateNavigators(page, numPages);
		updateTable(comm_list, page);
	}
	
	private void updateTable(Comm_List comm_list, int page) {
		Object data[][] = new Object[comm_list.size()][columns.length];
		for(int i = 0; i < data.length; i++){
			Communication comm = comm_list.get(i);
			data[i][0] = comm.getTitle();
			data[i][1] = comm.getDate();
			data[i][2] = comm.getType() == 0 ? "Incoming" : "Outgoing";
			data[i][3] = comm.getSender();
			data[i][4] = comm.getPath();
		}
		model = new DefaultTableModel(data, columns);
		tblSearch.setModel(model);
		System.gc();
	}

	private void updateNavigators(int page, int numPages) {
		btnPrev.setEnabled(page > 0);
		btnNext.setEnabled(page < numPages-1 && Communication.numCommunication > 0);
//		if(page < cbPage.getItemCount())
//			cbPage.setSelectedIndex(page);
	}
	
	@SuppressWarnings("unchecked")
	public void updateCBPage(){
		if(numPages == tmpNumPages)
			return;
		pageModel.removeAllElements();
		if(Communication.numCommunication == 0)
			return;
		String pages[] = new String[numPages];
		for(int i = 0; i < numPages; i++)
			pages[i] = ""+(i+1);
		pageModel = new DefaultComboBoxModel<String>(pages);
		cbPage.setModel(pageModel);
	}
}