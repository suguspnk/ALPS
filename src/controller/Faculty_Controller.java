package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import model.SQLConnection;
import model.faculty.Faculty;
import model.faculty.Faculty_List;
import model.faculty.entities.Activity;
import model.faculty.entities.Publication;
import model.faculty.entities.Subject;
import others.Util;
import view.faculty.Faculty_Profile;
import view.faculty.Faculty_Tab;
import view.faculty.forms.Faculty_Form;
import view.faculty.tabs.Activity_Tab;
import view.faculty.tabs.Publication_Tab;
import view.faculty.tabs.Subject_Tab;

public class Faculty_Controller implements ActionListener{
	

	private Faculty_List facultyList;
	public static Faculty faculty;
	public Faculty_Profile profile;
	private Faculty_Form facultyForm;
	public Faculty_Tab faculty_tab;
	Faculty_Profile_Controllers faculty_profile_controller;
	Activity_Tab activityTab;
	Publication_Tab publicationTab;
	Subject_Tab subjectTab;
	
	public Faculty_Controller(){
		
		init();
		
		addListener();
	}
	
	private void addListener(){
		
		//Faculty Tab
		faculty_tab.addBtn.addActionListener(this);
		faculty_tab.searchBtn.addActionListener(this);
		faculty_tab.search.addActionListener(this);
		faculty_tab.tbl.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				  int row = faculty_tab.tbl.rowAtPoint(e.getPoint());
			      int col = faculty_tab.tbl.columnAtPoint(e.getPoint());
			      if(col == 7 && row >= 0 && row<faculty_tab.tbl.getRowCount()){
			    	  faculty = facultyList.getRecord(row); 
			    	  profile.showProfile(faculty,row);
			      }
			  }
			});
		faculty_tab.pages.addActionListener(this);
		faculty_tab.prev.addActionListener(this);
		faculty_tab.next.addActionListener(this);
		
		//Faculty Form
		facultyForm.btnAdd.addActionListener(this);
		facultyForm.btnCancel.addActionListener(this);
		facultyForm.ch_img.addActionListener(this);
		
		//Faculty Profile
		profile.Edit.addActionListener(this);
		profile.Save.addActionListener(this);
		profile.Delete.addActionListener(this);
		profile.Cancel.addActionListener(this);
		profile.ch_img.addActionListener(this);
		
		profile.tPane.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if(profile.tPane.getSelectedIndex() == 2){
					activityTab.checkAll.setSelected(false);
					try {
						activityTab.updateTable(Activity.getActivities(faculty.id));
					} catch (ClassNotFoundException | SQLException e1) {
						JOptionPane.showMessageDialog(profile, "An error occurred while getting the activities. The error message is: "+e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						e1.printStackTrace();
					}
				}
				else if(profile.tPane.getSelectedIndex() == 1){
					publicationTab.checkAll.setSelected(false);
					try {
						publicationTab.updateTable(Publication.getPublications(faculty.id));
					} catch (ClassNotFoundException | SQLException e1) {
						JOptionPane.showMessageDialog(profile, "An error occurred while getting the publications. The error message is: "+e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						e1.printStackTrace();
					}
				}
				else if(profile.tPane.getSelectedIndex() == 0){
					subjectTab.checkAll.setSelected(false);
					try {
						System.out.println("c"+faculty.id);
						subjectTab.updateTable(Subject.getSubjects(faculty.id));
					} catch (ClassNotFoundException | SQLException e1) {
						JOptionPane.showMessageDialog(profile, "An error occurred while getting the subjects. The error message is: "+e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						e1.printStackTrace();
					}
				}
			}
		});
		
		faculty_profile_controller = Faculty_Profile_Controllers.getInstance();
		activityTab = faculty_profile_controller.activityTab;
		publicationTab = faculty_profile_controller.publicationTab;
		subjectTab = faculty_profile_controller.subjectTab;
	}
	
	public void init(){
		faculty = new Faculty();
		faculty_tab = Faculty_Tab.getInstance();
		facultyList = Faculty_List.getInstance();
		profile = Faculty_Profile.getInstance();
		facultyForm = Faculty_Form.getInstance();
		
		updatePageNum(0);
		facultyList.update_FacultyList();
		faculty_tab.update(facultyList.facList);
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == faculty_tab.addBtn)
			facultyForm.showFaculty_Form();
		
		if(e.getSource() == faculty_tab.searchBtn || e.getSource() == faculty_tab.search){
			updatePageNum(faculty_tab.options.getSelectedItem().toString().equals("All")? 0 : 1);
			search();
		}
		
		if(e.getSource() == faculty_tab.pages){
			facultyList.page = faculty_tab.pages.getSelectedIndex();
			faculty_tab.next.setEnabled(facultyList.page < faculty_tab.numPages-1);
			faculty_tab.prev.setEnabled(facultyList.page+1 > 1);
			changePage();
		}
		
		if(e.getSource() == faculty_tab.prev){
			facultyList.page = facultyList.page - 1;
			faculty_tab.pages.setSelectedIndex(facultyList.page);
			faculty_tab.next.setEnabled(facultyList.page < faculty_tab.numPages-1);
			faculty_tab.prev.setEnabled(facultyList.page+1 > 1);
		}
		
		if(e.getSource() == faculty_tab.next){
			facultyList.page = facultyList.page + 1;
			faculty_tab.pages.setSelectedIndex(facultyList.page);
			faculty_tab.next.setEnabled(facultyList.page < faculty_tab.numPages-1);
			faculty_tab.prev.setEnabled(facultyList.page+1 > 1);
		}
			
		if(e.getSource() == facultyForm.btnCancel)
			facultyForm.setVisible(false);
		
		if(e.getSource() == facultyForm.btnAdd){
			add();
			updatePageNum(0);
		}
		
		if(e.getSource() == facultyForm.ch_img)
			facultyForm.setImage();
		
		if(e.getSource() == profile.Edit)
			profile.showFields(true);
		
		if(e.getSource() == profile.Cancel){
			profile.initFields();
			profile.showFields(false);
		}
		
		if(e.getSource() == profile.Save){
			edit();
			updatePageNum(0);
		}
		
		if(e.getSource() == profile.Delete){
			delete();
			updatePageNum(0);
		}
		
		if(e.getSource() == profile.ch_img)
			profile.changeImage();
	}
	
	
	//Adding a new faculty Record 
	private void add(){
		
		if(!facultyForm.verifyInput())
			return;
		
		
		String fName = facultyForm.fName.getText().trim();
		String mName = facultyForm.mName.getText().trim();
		String lName = facultyForm.lName.getText().trim();
		String email = facultyForm.email.getText().trim();
		
		//check if faculty exist
		if(faculty.checkIF_FacultyExist(fName,mName,lName)){
			JOptionPane.showMessageDialog(facultyForm, "Faculty Already Exist!", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if(faculty.checkIF_EmailExists(email, -1)){
			JOptionPane.showMessageDialog(facultyForm, "Email Already Exists!", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		String addr = facultyForm.Address.getText().trim();
		java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyy-MM-dd");
		String bdate = fmt.format(facultyForm.bdate.getDate());
		String conOne = facultyForm.contactOne.getText().trim();
		String conTwo = facultyForm.contactTwo.getText().trim();
		
		if(conOne.isEmpty()&&!conTwo.isEmpty()){
			conOne = conTwo;
			conTwo = "";
		}
		
		String degree = facultyForm.degree.getText().trim();
		String position = facultyForm.position.getText().trim();
		String status = facultyForm.status.getSelectedItem().toString();
		
		String path = !facultyForm.hasChosen? "" : Util.getInstance().moveImage(facultyForm.file, lName, fName, mName);
		facultyForm.hasChosen = false;
		
		if(faculty.addOrUpdate(1, fName, mName, lName, addr, bdate, conOne,conTwo, email, degree, position, status,path,0))
			JOptionPane.showMessageDialog(facultyForm, faculty.prompt, "Success", JOptionPane.INFORMATION_MESSAGE);
		else{
			JOptionPane.showMessageDialog(facultyForm, faculty.prompt, "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		facultyForm.setVisible(false);
		
		updateTab();
	}
	
	//Edit a faculty Record
	private void edit(){
		
		if(!profile.verifyInput())
			return;
		
		String fName = profile.fName.getText().trim();
		String mName = profile.mName.getText().trim();
		String lName = profile.lName.getText().trim();
		String email = profile.email.getText().trim();
		
		//check if faculty exist
		if((!fName.equals(profile.getFaculty().fName) && !lName.equals(profile.getFaculty().lName)
			&& !mName.equals(profile.getFaculty().mName)) && faculty.checkIF_FacultyExist(fName,mName,lName)){
			JOptionPane.showMessageDialog(profile, "Another Faculty with the Same Name Already Exist!", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if(faculty.checkIF_EmailExists(email, faculty.id)){
			JOptionPane.showMessageDialog(facultyForm, "Email Already Exists!", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		String addr = profile.Address.getText().trim();
		java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyy-MM-dd");
		String bdate = fmt.format(profile.bdate.getDate());
		String conOne = profile.contactOne.getText().trim();
		String conTwo = profile.contactTwo.getText().trim();
		
		if(conOne.isEmpty()&&!conTwo.isEmpty()){
			conOne = conTwo;
			conTwo = "";
		}
		
		String degree = profile.degree.getText().trim();
		String position = profile.position.getText().trim();
		String status = profile.status.getSelectedItem().toString();
		
		String path = !profile.hasChosen? "" : Util.getInstance().moveImage(profile.file, lName, fName, mName);
		profile.hasChosen = false;
		
		if(faculty.addOrUpdate(2, fName, mName, lName, addr, bdate, conOne,conTwo, email, degree, position, status, path, profile.current_row))
			JOptionPane.showMessageDialog(profile, faculty.prompt, "Success", JOptionPane.INFORMATION_MESSAGE);
		else
			JOptionPane.showMessageDialog(profile, faculty.prompt, "Error", JOptionPane.ERROR_MESSAGE);
		

		updateTab();
		
		profile.setVisible(false);
		
	}
	
	//Delete or deactivate faculty
	private void delete(){
		
		if(JOptionPane.showConfirmDialog(profile, "Are you sure you want to delete " + facultyList.facList.get(profile.current_row).lName + ", " 
		   + facultyList.facList.get(profile.current_row).fName+ " " + facultyList.facList.get(profile.current_row).mName +"?","Delete Faculty Record?", 
		   JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION){
			
			if(faculty.deactivate(facultyList.facList.get(profile.current_row).id))
				JOptionPane.showMessageDialog(profile, faculty.prompt, "Success", JOptionPane.INFORMATION_MESSAGE);
			else
				JOptionPane.showMessageDialog(profile, faculty.prompt, "Error", JOptionPane.ERROR_MESSAGE);

			updateTab();
			
			profile.setVisible(false);
		}
	}
	
	private void search(){

		if(faculty_tab.options.getSelectedItem().toString().equals("All")){
			faculty_tab.search.setText("");
			facultyList.update_FacultyList();
		}
		
		else{
			if(faculty_tab.search.getText().trim().isEmpty())
				JOptionPane.showMessageDialog(faculty_tab, "Input search key", "Error", JOptionPane.ERROR_MESSAGE);
			else{
				facultyList.update_searchList();
			}
		}
		
		faculty_tab.update(facultyList.facList);
	}
	
	private void changePage(){
		
		if(facultyList.searchOpt.equals("All"))
			facultyList.update_FacultyList();
		else
			facultyList.update_searchList();

			
		faculty_tab.update(facultyList.facList);	
	}
	
	private void updateTab(){

		facultyList.update_FacultyList();
		faculty_tab.update(facultyList.facList);
		if(faculty_tab.pages.getItemCount()!=0){
			facultyList.page = 0;
			faculty_tab.options.setSelectedIndex(0);
		}
		faculty_tab.search.setText("");
	}
	
	@SuppressWarnings("static-access")
	public void updatePageNum(int ch){
		
		String query = "";
		faculty_tab.pageModel.removeAllElements();
		facultyList.page = 0;
		if(ch == 0){
			facultyList.searchOpt = "All";
			faculty_tab.updatePages(facultyList.get_NumPages("SELECT Count(id) as count FROM faculty"));
		}
		
		else if(ch == 1){
			String searchOpt = "",opt = faculty_tab.options.getSelectedItem().toString(),
					key = faculty_tab.search.getText().trim();
			if(opt.equals("Last Name"))
				searchOpt = "surname";
			else if(opt.equals("First Name"))
				searchOpt = "first_name";
			else if(opt.equals("Middle Name"))
				searchOpt = "middle_name";
			else if(opt.equals("Address"))
				searchOpt = "address";
			else if(opt.equals("Birthdate"))
				searchOpt = "birthdate";
			else if(opt.equals("Email"))
				searchOpt = "email";
			else if(opt.equals("Contact #"))
				searchOpt = "number";
			
			if(searchOpt.equals("number"))
				query = "SELECT Count(faculty.id) as count FROM faculty,contact_number WHERE number = '" + SQLConnection.getInstance().insertBackSlash(key) + "' AND faculty.id = contact_number.faculty_id";
			else if(searchOpt.equals("email"))
				query = "SELECT Count(faculty.id) as count FROM faculty,email WHERE email LIKE '%" + SQLConnection.getInstance().insertBackSlash(key) + "%' AND faculty.id = email.faculty_id";
			else
				query = "SELECT Count(id) as count FROM faculty WHERE " + searchOpt  + " LIKE '%" + SQLConnection.getInstance().insertBackSlash(key) + "%'";
			
			facultyList.searchOpt = searchOpt;
			facultyList.key = key;
			
			faculty_tab.updatePages(facultyList.get_NumPages(query));
		}
		
	}

}
