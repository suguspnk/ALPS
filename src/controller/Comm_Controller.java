package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import model.SQLConnection;
import model.comm.Comm_List;
import model.comm.Communication;
import view.comm.Comm_Form;
import view.comm.Comm_Tab;

public class Comm_Controller implements ActionListener, ItemListener, MouseListener{
	
	int row = -1;
	Comm_List comm_list;
	Communication communication;
	Comm_Tab comm_tab;
	public Comm_Form comm_form;
	
	public static SQLConnection sqlConnect;
	
	public Comm_Controller() {
		comm_list = new Comm_List();
		communication = new Communication();
		comm_tab = new Comm_Tab();
		comm_form = new Comm_Form();
		addListener();
		sqlConnect = SQLConnection.getInstance();
	}

	private void addListener() {
		 comm_form.btnBrowse.addActionListener(this);
		 comm_form.btnAdd.addActionListener(this);
		 comm_form.btnCancel.addActionListener(this);
		 comm_form.btnUpdate.addActionListener(this);
		 comm_form.btnDelete.addActionListener(this);
		 
		 comm_tab.btnAdd.addActionListener(this);
		 comm_tab.btnSearch.addActionListener(this);
		 comm_tab.tfSearch.addActionListener(this);
		 
		 comm_tab.tblSearch.addMouseListener(this);
		 comm_tab.comboBox.addItemListener(this);
		 comm_tab.cbPage.addActionListener(this);
		 
		 comm_tab.btnPrev.addActionListener(this);
		 comm_tab.btnNext.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getSource() == comm_form.btnBrowse) {
			String filepath = comm_form.chooseFile();
			if(filepath != null)
				comm_form.tfFilePath.setText(filepath);
		}
		
		else if(e.getSource() == comm_form.btnAdd) {
			if (comm_form.verifyInput()) {
				try {
					comm_form.setCommunicationValues(communication);
					communication.add(communication.getType(), communication.getSender(), communication.getDate(), communication.getPath(), communication.getTitle(), Home_Controller.user.getId());
					JOptionPane.showMessageDialog(comm_form, "The communication was successfully added.", "Communication Added", JOptionPane.INFORMATION_MESSAGE);
					comm_form.setVisible(false);
					comm_form.clearFields();
					showAll();
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(Home_Controller.homeFrame, "An error occurred while adding the communication to the database. The error message is: "+e1.getMessage(), "Communication Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
			}
			else
				JOptionPane.showMessageDialog(comm_form, "Please fill out the required fields.", "Add Communication Error", JOptionPane.ERROR_MESSAGE);
		}
		
		else if(e.getSource() == comm_form.btnCancel) {
			comm_form.setVisible(false);
		}
		
		else if (e.getSource() == comm_form.btnUpdate) {
			if (comm_form.verifyInput()) {
				try {
					comm_form.setCommunicationValues(communication);
					communication.updateCommunication(comm_list.get(row).getId(), communication.getDate(), communication.getTitle(), communication.getType(), communication.getSender(), communication.getPath());
					JOptionPane.showMessageDialog(comm_form, "The communication record was successfully updated.", "Communication Update", JOptionPane.INFORMATION_MESSAGE);
					comm_form.setVisible(false);
					comm_form.clearFields();
					
					showAll();
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(Home_Controller.homeFrame, "An error occurred while updating the communication record to the database. The error message is: "+e1.getMessage(), "Communication Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
			}
			else
				JOptionPane.showMessageDialog(comm_form, "Please fill out the required fields.", "Update Communication Error", JOptionPane.ERROR_MESSAGE);
		}
		
		else if (e.getSource() == comm_form.btnDelete) {
			 int choice = JOptionPane.showConfirmDialog(comm_form, "Are you sure you want to delete this record from the database?", "Remove", JOptionPane.YES_NO_OPTION);
             if(choice==0){
            	 try {
					SQLConnection.getInstance().update("delete from communication where id = "+comm_list.get(row).getId()+";");
					JOptionPane.showMessageDialog(null, "Communication record successfully deleted!", "Success", JOptionPane.INFORMATION_MESSAGE);
					comm_form.setVisible(false);
					comm_form.clearFields();
					
					showAll();
            	} catch (Exception e1) {
					e1.printStackTrace();
				}
             }
		}
		
		else if(e.getSource() == comm_tab.btnAdd){
			comm_form.btnAdd.setVisible(true);
			comm_form.btnCancel.setVisible(true);
			comm_form.btnUpdate.setVisible(false);
			comm_form.btnDelete.setVisible(false);

			comm_form.setVisible(true);
		}
		else if((e.getSource() == comm_tab.btnSearch || e.getSource() == comm_tab.tfSearch) && !comm_tab.tfSearch.getText().trim().isEmpty()) {
			comm_tab.page = 0;
//			System.out.println("search");
			search(comm_tab.comboBox.getSelectedIndex(), comm_tab.tfSearch.getText().trim(), offset);
			comm_tab.updateCBPage();
		}
		
		else if (e.getSource() == comm_tab.btnPrev) {
			comm_tab.page--;
			comm_tab.cbPage.setSelectedIndex(comm_tab.page);
//			search(searchIndex, inputSearchString, comm_tab.page*15);
		}
		
		else if (e.getSource() == comm_tab.btnNext) {
			comm_tab.page++;
			comm_tab.cbPage.setSelectedIndex(comm_tab.page);
//			search(searchIndex, inputSearchString, comm_tab.page*15);
		}
		
		else if(e.getSource() == comm_tab.cbPage && comm_tab.cbPage.getItemCount() > 0){
			comm_tab.page = comm_tab.cbPage.getSelectedIndex();
//			System.out.println("cbPage"+comm_tab.cbPage.getItemCount());
			if(comm_tab.page >= 0)
				search(searchIndex, inputSearchString, comm_tab.page*15);
		}
	}
	
	private void showAll() {
		comm_tab.page = 0;
		search(0, comm_tab.tfSearch.getText().trim(), 0);
		comm_tab.updateCBPage();
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if(e.getSource() == comm_tab.comboBox){
			if (comm_tab.comboBox.getSelectedIndex() == 0) {
				comm_tab.tfSearch.setText("");
				comm_tab.tfSearch.setEnabled(false);
				comm_tab.btnSearch.setEnabled(false);
	
				showAll();
			}
			else{
				comm_tab.tfSearch.setEnabled(true);
				comm_tab.btnSearch.setEnabled(true);
			}
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent evt) {
		row =evt.getY()/comm_tab.tblSearch.getRowHeight();
        if(evt.getClickCount() == 2 && row < comm_list.size()){
        	comm_form.setValues(""+comm_tab.tblSearch.getValueAt(row, 0), ""+comm_tab.tblSearch.getValueAt(row, 2), ""+comm_tab.tblSearch.getValueAt(row, 3), ""+comm_tab.tblSearch.getValueAt(row, 4));
			
        	comm_form.btnAdd.setVisible(false);
			comm_form.btnCancel.setVisible(false);
			comm_form.btnUpdate.setVisible(true);
			comm_form.btnDelete.setVisible(true);
			
        	comm_form.setVisible(true);
        }
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {}

	int offset, searchIndex;
	String inputSearchString;
	public void search(int selectedSearchIndex, String inputSearchString, int offset) {
		try {
			this.offset = offset;
			searchIndex = selectedSearchIndex;
			this.inputSearchString = inputSearchString;
			Comm_Tab.tmpNumPages = Comm_Tab.numPages;
			comm_list.updateList(selectedSearchIndex, inputSearchString, offset);
			comm_tab.updateGUI(comm_list);
			if(comm_list.size()==0) {
				comm_tab.pageModel.removeAllElements();
				JOptionPane.showMessageDialog(null, "No result(s) found.", "Search Prompt", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(Home_Controller.homeFrame, "An error occurred while retrieving the communication list. The error message is: "+e.getMessage(), "Communication List Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
}