package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import model.faculty.entities.Activity;
import model.faculty.entities.Publication;
import model.faculty.entities.Subject;
import others.Util.TableCheckBoxListener;
import view.faculty.Faculty_Profile;
import view.faculty.forms.Activity_Form;
import view.faculty.forms.Publication_Form;
import view.faculty.forms.Subject_Form;
import view.faculty.tabs.Activity_Tab;
import view.faculty.tabs.Publication_Tab;
import view.faculty.tabs.Subject_Tab;

public class Faculty_Profile_Controllers {

	private static Faculty_Profile_Controllers instance;
	
	public Subject_Controller subject_controller;
	public Publication_Controller publication_controller;
	public Activity_Controller activity_controller;

	public Activity_Tab activityTab;
	public Activity_Form activityForm;
	public Publication_Tab publicationTab;
	public Publication_Form publicationForm;
	public Subject_Tab subjectTab;
	public Subject_Form subjectForm;
	public Faculty_Profile profile;
	private TableCheckBoxListener subjectCheckboxListener;
	private TableCheckBoxListener activityCheckboxListener;
	private TableCheckBoxListener publicationCheckboxListener;
	
	private Activity activity;
	private Publication publication;
	private	Subject subject;
	
	public Faculty_Profile_Controllers() {
		
		activityForm = Activity_Form.getInstance();
		publicationForm = Publication_Form.getInstance();
		subjectForm = Subject_Form.getInstance();
		
		activityTab = Activity_Tab.getInstance();
		publicationTab = Publication_Tab.getInstance();
		subjectTab = Subject_Tab.getInstance();
		
		subjectCheckboxListener = new TableCheckBoxListener(subjectTab.subjectTable, subjectTab.checkAll, 0);
		subjectTab.subjectTable.addMouseListener(subjectCheckboxListener);
		subjectTab.checkAll.addMouseListener(subjectCheckboxListener);
		
		activityCheckboxListener = new TableCheckBoxListener(activityTab.activityTable, activityTab.checkAll, 0);
		activityTab.activityTable.addMouseListener(activityCheckboxListener);
		activityTab.checkAll.addMouseListener(activityCheckboxListener);
		
		publicationCheckboxListener = new TableCheckBoxListener(publicationTab.publicationTable, publicationTab.checkAll, 0);
		publicationTab.publicationTable.addMouseListener(publicationCheckboxListener);
		publicationTab.checkAll.addMouseListener(publicationCheckboxListener);
		
		subject_controller = new Subject_Controller();
		publication_controller = new Publication_Controller();
		activity_controller = new Activity_Controller();
		
		profile = Faculty_Profile.getInstance();
	}
	
	class Activity_Controller implements ActionListener{

		public Activity_Controller(){
			activityForm.btnAdd.addActionListener(this);
			activityForm.btnUpdate.addActionListener(this);
			activityForm.btnCancel.addActionListener(this);
			activityTab.addButton.addActionListener(this);
			activityTab.editButton.addActionListener(this);
			activityTab.deleteButton.addActionListener(this);
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == activityTab.addButton)
				activityForm.showActivityForm(0, null);
			else if(e.getSource() == activityForm.btnAdd)
				addOrUpdateActivity(0);
			else if(e.getSource() == activityForm.btnCancel)
				activityForm.setVisible(false);
			else if(e.getSource() == activityTab.editButton)
				edit();
			else if(e.getSource() == activityTab.deleteButton)
				deleteActivities();
			else if(e.getSource() == activityForm.btnUpdate)
				addOrUpdateActivity(1);
		}
		
		private void edit() {
			int selectedRow = activityTab.activityTable.getSelectedRow();
			if(selectedRow == -1){
				JOptionPane.showMessageDialog(profile, "There is no selected row!", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			activity = Activity.list.get(selectedRow);
			activityForm.showActivityForm(1, activity);
		}

		private void deleteActivities() {
			if(activityCheckboxListener.counter == 0){
				JOptionPane.showMessageDialog(profile, "There are no checked rows.", "Message", JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			if(JOptionPane.showConfirmDialog(profile, "Are you sure you want to delete the checked rows?", "Confirm", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
				return;
			for(int i = activityTab.activityTable.getRowCount()-1; i >= 0; i--){
				if(!(Boolean)activityTab.activityTable.getValueAt(i, 0))
					continue;
				Activity activity = Activity.list.get(i);
				try {
					activity.removeFromDatabase();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(profile, "Failed to delete row "+(i+1)+"! The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
					return;
				}
				Activity.list.remove(i);
			}
			activityCheckboxListener.setCheckedRows(0);
			activityTab.updateTable(Activity.list);
			JOptionPane.showMessageDialog(profile, "The rows where successfully removed.", "Success", JOptionPane.INFORMATION_MESSAGE);
		}

		private void addOrUpdateActivity(int type) {
			if(!activityForm.verifyInput())
				return;
			if(type == 0)
				activity = new Activity(0, Faculty_Controller.faculty.id, activityForm.getVenue(),activityForm.getActivityTitle(),activityForm.getActivityType(), activityForm.getDate());
			else
				activity.setValues(activity.id, activity.facultyId, activityForm.getVenue(), activityForm.getActivityTitle(), activityForm.getActivityType(), activityForm.getDate());
			if(activity.addOrUpdate(type))
				JOptionPane.showMessageDialog(activityForm, activity.prompt, "Success!", JOptionPane.INFORMATION_MESSAGE);
			else{
				JOptionPane.showMessageDialog(activityForm, activity.prompt, "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			activityForm.clear();
			activityForm.setVisible(false);
			try {
				activityTab.updateTable(Activity.getActivities(Faculty_Controller.faculty.id));
			} catch (ClassNotFoundException | SQLException e) {
				JOptionPane.showMessageDialog(profile, "An error occured while retrieving the list of faculty's activities. The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}
	
	class Subject_Controller implements ActionListener{
		JFileChooser chooser;
		
		public Subject_Controller(){
			subjectForm.btnAdd.addActionListener(this);
			subjectForm.btnUpdate.addActionListener(this);
			subjectForm.btnCancel.addActionListener(this);
			subjectForm.browse.addActionListener(this);
			subjectTab.addButton.addActionListener(this);
			subjectTab.editButton.addActionListener(this);
			subjectTab.deleteButton.addActionListener(this);
			chooser = new JFileChooser();
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			chooser.setMultiSelectionEnabled(false);
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == subjectTab.addButton)
				subjectForm.showSubjectForm(0, null);
			else if(e.getSource() == subjectForm.btnAdd)
				addOrUpdateSubject(0);
			else if(e.getSource() == subjectForm.btnCancel)
				subjectForm.setVisible(false);
			else if(e.getSource() == subjectTab.editButton)
				edit();
			else if(e.getSource() == subjectTab.deleteButton)
				deleteSubjects();
			else if(e.getSource() == subjectForm.btnUpdate)
				addOrUpdateSubject(1);
			else if(e.getSource() == subjectForm.browse)
				uploadFile();
		}
		
		private void uploadFile() {
			if(chooser.showOpenDialog(subjectForm) == JFileChooser.APPROVE_OPTION)
				subjectForm.uploadField.setText(chooser.getSelectedFile().getAbsolutePath().replace("\\", "/"));
		}
		
		private void edit() {
			int selectedRow = subjectTab.subjectTable.getSelectedRow();
			if(selectedRow == -1){
				JOptionPane.showMessageDialog(profile, "There is no selected row!", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			subject = Subject.list.get(selectedRow);
			subjectForm.showSubjectForm(1, subject);
		}

		private void deleteSubjects() {
			if(subjectCheckboxListener.counter == 0){
				JOptionPane.showMessageDialog(profile, "There are no checked rows.", "Message", JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			if(JOptionPane.showConfirmDialog(profile, "Are you sure you want to delete the checked rows?", "Confirm", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
				return;
			for(int i = subjectTab.subjectTable.getRowCount()-1; i >= 0; i--){
				if(!(Boolean)subjectTab.subjectTable.getValueAt(i, 0))
					continue;
				Subject subject = Subject.list.get(i);
				try {
					subject.removeFromDatabase();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(profile, "Failed to delete row "+(i+1)+"! The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
					return;
				}
				Subject.list.remove(i);
			}
			subjectCheckboxListener.setCheckedRows(0);
			subjectTab.updateTable(Subject.list);
			JOptionPane.showMessageDialog(profile, "The rows where successfully removed.", "Success", JOptionPane.INFORMATION_MESSAGE);
		}

		private void addOrUpdateSubject(int type) {
			if(!subjectForm.verifyInput())
				return;
			if(type == 0)
				subject = new Subject(0, subjectForm.getSubjectTitle(), subjectForm.getTime(), subjectForm.getRoom(), subjectForm.getSyllabus(), subjectForm.getSem(), Faculty_Controller.faculty.id);
			else
				subject.setValues(subject.id, subjectForm.getSubjectTitle(), subjectForm.getTime(), subjectForm.getRoom(), subjectForm.getSyllabus(), subjectForm.getSem(), Faculty_Controller.faculty.id);
			if(subject.addOrUpdate(type))
				JOptionPane.showMessageDialog(subjectForm, subject.prompt, "Success!", JOptionPane.INFORMATION_MESSAGE);
			else{
				JOptionPane.showMessageDialog(subjectForm, subject.prompt, "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			subjectForm.clear();
			subjectForm.setVisible(false);
			try {
				subjectTab.updateTable(Subject.getSubjects(Faculty_Controller.faculty.id));
			} catch (ClassNotFoundException | SQLException e) {
				JOptionPane.showMessageDialog(profile, "An error occured while retrieving the list of faculty's subjects. The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}

	class Publication_Controller implements ActionListener{

		JFileChooser chooser;
		public Publication_Controller() {
			publicationForm.btnAdd.addActionListener(this);
			publicationForm.btnUpdate.addActionListener(this);
			publicationForm.btnCancel.addActionListener(this);
			publicationForm.browse.addActionListener(this);
			publicationTab.addButton.addActionListener(this);
			publicationTab.editButton.addActionListener(this);
			publicationTab.deleteButton.addActionListener(this);
			chooser = new JFileChooser();
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			chooser.setMultiSelectionEnabled(false);
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == publicationTab.addButton)
				publicationForm.showPublicationForm(0, null);
			else if(e.getSource() == publicationForm.btnAdd)
				addOrUpdatePublication(0);
			else if(e.getSource() == publicationForm.btnCancel)
				publicationForm.setVisible(false);
			else if(e.getSource() == publicationTab.editButton)
				edit();
			else if(e.getSource() == publicationTab.deleteButton)
				deletePublications();
			else if(e.getSource() == publicationForm.btnUpdate)
				addOrUpdatePublication(1);
			else if(e.getSource() == publicationForm.browse)
				uploadFile();
		}
		
		private void uploadFile() {
			if(chooser.showOpenDialog(publicationForm) == JFileChooser.APPROVE_OPTION)
				publicationForm.uploadField.setText(chooser.getSelectedFile().getAbsolutePath().replace("\\", "/"));
		}

		private void edit() {
			int selectedRow = publicationTab.publicationTable.getSelectedRow();
			if(selectedRow == -1){
				JOptionPane.showMessageDialog(profile, "There is no selected row!", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			publication = Publication.list.get(selectedRow);
			publicationForm.showPublicationForm(1, publication);
		}

		private void deletePublications() {
			if(publicationCheckboxListener.counter == 0){
				JOptionPane.showMessageDialog(profile, "There are no checked rows.", "Message", JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			if(JOptionPane.showConfirmDialog(profile, "Are you sure you want to delete the checked rows?", "Confirm", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
				return;
			for(int i = publicationTab.publicationTable.getRowCount()-1; i >= 0; i--){
				if(!(Boolean)publicationTab.publicationTable.getValueAt(i, 0))
					continue;
				Publication publication = Publication.list.get(i);
				try {
					publication.removeFromDatabase();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(profile, "Failed to delete row "+(i+1)+"! The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
					return;
				}
				Publication.list.remove(i);
			}
			publicationCheckboxListener.setCheckedRows(0);
			publicationTab.updateTable(Publication.list);
			JOptionPane.showMessageDialog(profile, "The rows where successfully removed.", "Success", JOptionPane.INFORMATION_MESSAGE);
		}

		private void addOrUpdatePublication(int type) {
			if(!publicationForm.verifyInput())
				return;
			if(type == 0)
				publication = new Publication(0, publicationForm.getPublicationType(), publicationForm.getPublicationTitle(), publicationForm.getDate_Published(), publicationForm.getFilePath(), Faculty_Controller.faculty.id);
			else
				publication.setValues(publication.id, publication.facultyId, publicationForm.getFilePath(), publicationForm.getPublicationTitle(), publicationForm.getPublicationType(), publicationForm.getDate_Published());
			if(publication.addOrUpdate(type))
				JOptionPane.showMessageDialog(publicationForm, publication.prompt, "Success!", JOptionPane.INFORMATION_MESSAGE);
			else{
				JOptionPane.showMessageDialog(publicationForm, publication.prompt, "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			publicationForm.clear();
			publicationForm.setVisible(false);
			try {
				publicationTab.updateTable(Publication.getPublications(Faculty_Controller.faculty.id));
			} catch (ClassNotFoundException | SQLException e) {
				JOptionPane.showMessageDialog(profile, "An error occured while retrieving the list of faculty's subjects. The error message is: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}

	public static Faculty_Profile_Controllers getInstance() {
		if(instance == null)
			instance = new Faculty_Profile_Controllers();
		return instance;
	}
	
}