package controller;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import view.Home_Frame;
import view.Login_Form;
import view.additionalFunctionalities.About;
import view.additionalFunctionalities.Create_User_Frame;
import view.additionalFunctionalities.Help;
import model.Config;
import model.SQLConnection;
import model.User;
import model.faculty.Faculty_List;

public class Home_Controller implements ActionListener, ChangeListener{
	Faculty_Controller facultyControl;
	Report_Controller reportControl;
	Comm_Controller commControl;
	UserProfile_Controller userControl;
	Create_User_Frame createUserFrame;
	
	public static Login_Form login;
	public static SQLConnection sqlConnect = SQLConnection.getInstance();
	public static User user = new User();
	public static Home_Frame homeFrame;
	
	public Home_Controller(){
		init();
		createDB();
		checkIfThereIsAUser();
	}

	private void checkIfThereIsAUser() {
		try {
			if(User.hasUsers())
				login.setVisible(true);
			else
				createUserFrame.setVisible(true);
		} catch (ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(Home_Controller.homeFrame, 
					"An error occured when checking if users already exists!", "MySQL Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	private void createDB() {
		try {
			sqlConnect.createDB();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(homeFrame, "The file for creating the database does not exist!"
					+ " Please make sure that the database.sql exists in the \"db\" folder inside the application folder.",
					"Database file not found!", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(homeFrame, "An error occured when creating the database! The error message is: "+e.getMessage(),
					"Database Creation Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(homeFrame, "An error occured when fetching the JDBC driver! The error message is: "+e.getMessage(),
					"JDBC Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (SQLException e) {
			if(e.getMessage().contains("Duplicate entry"));
			else{
				JOptionPane.showMessageDialog(homeFrame, "An error in MySQL! The error message is: "+e.getMessage(),
					"Database Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}

	private void init() {
		initController();
		
		initView();
		
		addListener();
	}


	private void addListener() {
		login.btnLogin.addActionListener(this);
		login.passwordPF.addActionListener(this);
		login.usernameTF.addActionListener(this);
		
		homeFrame.btnEdit.addActionListener(this);
		homeFrame.btnAbout.addActionListener(this);
		homeFrame.btnHelp.addActionListener(this);
		homeFrame.btnLogout.addActionListener(this);
		homeFrame.btnBackup.addActionListener(this);
		
		homeFrame.tabbedPane.addChangeListener(this);
		
		CloseApplicationListener closeListener = new CloseApplicationListener();
		homeFrame.addWindowListener(closeListener);
		login.addWindowListener(closeListener);
	}

	private void initController() {
		facultyControl = new Faculty_Controller();
		reportControl = new Report_Controller();
		commControl = new Comm_Controller();
		userControl = new UserProfile_Controller();
	}


	private void initView() {
		createUserFrame = new Create_User_Frame();
		
		login = new Login_Form();
		
		homeFrame = new Home_Frame();

		homeFrame.tabbedPane.addTab("Profile", facultyControl.faculty_tab);
		homeFrame.tabbedPane.addTab("Communications", commControl.comm_tab);
		homeFrame.tabbedPane.addTab("Reports", reportControl.report_Tab);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == login.usernameTF)
			transferFocusToPassword();
		else if(e.getSource() == login.passwordPF || e.getSource() == login.btnLogin)
			login();
		else if(e.getSource() == homeFrame.btnEdit)
			editProfile();
		else if(e.getSource() == homeFrame.btnAbout)
			about();
		else if(e.getSource() == homeFrame.btnHelp)
			help();
		else if(e.getSource() == homeFrame.btnLogout)
			confirmLogout();
		else if(e.getSource() == homeFrame.btnBackup)
			backupFunction();
	}
	
	private void backupFunction() {
		String path = chooseLocation();
		if(path != null)
			createBackup(path);
	}

	JFileChooser fileChooser;
	private String chooseLocation() {
		if(fileChooser == null)
			initFileChooser();
		if(fileChooser.showSaveDialog(homeFrame) == JFileChooser.APPROVE_OPTION){
			File selectedFile = fileChooser.getSelectedFile();
			if(selectedFile.exists()){
				if(JOptionPane.showConfirmDialog(homeFrame, "Overwrite existing file?", "File Already Exists", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
					return selectedFile.getAbsolutePath();
			}
			else
				return selectedFile.getAbsolutePath();
		}
		return null;
	}

	public static void createBackup(String path) {
		ResultSet rs = null;
		if(!path.endsWith(".sql"))
			path+=".sql";
		path = path.replace("\\", "/");
		try {
			rs = sqlConnect.query("select @@basedir");
			rs.next();
			String basedir = rs.getString(1);
			String bindir = Config.operating_system.equalsIgnoreCase("linux") ? "" : basedir+(!basedir.endsWith("/")?"/":"")+"bin/";
			boolean success = backupDataWithDatabase(bindir+"mysqldump", Config.hostname, Config.port, Config.user, Config.password, Config.database, path);
			if(success)
				JOptionPane.showMessageDialog(homeFrame, "The backup file was saved in "+path+".", "Backup Successful", JOptionPane.INFORMATION_MESSAGE);
			else
				JOptionPane.showMessageDialog(homeFrame, "Failed to save the backup file in "+path+".", "Backup Failed", JOptionPane.ERROR_MESSAGE);
		} catch (ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(homeFrame, "An error occured when getting the information about the location of mysql.", "MySQL Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (InterruptedException | IOException e) {
			JOptionPane.showMessageDialog(homeFrame, "An error occured when creating the backup file.", "MySQL Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	public static boolean backupDataWithDatabase(String dumpExePath, String host, String port, String user, String password, String database, String backupPath) throws IOException, InterruptedException {
        boolean status = false;
        Process p = null;
 
        String batchCommand[];
        if (password != "") {
            //Backup with database
        	batchCommand = new String[]{dumpExePath, "-h", host , "--port" , port , "-u" , user , "--password="+ password , "--add-drop-database", "-B" , database,"-r", backupPath};
        } else {
            batchCommand = new String[]{dumpExePath, "-h", host , "--port" , port , "-u" , user , "--add-drop-database", "-B" , database,"-r", backupPath};
        }
 
        Runtime runtime = Runtime.getRuntime();
        p = runtime.exec(batchCommand);
        int processComplete = p.waitFor();
        InputStream is = p.getErrorStream();
        byte arr[] = new byte[500];
        is.read(arr);
        for(int i = 0; i < arr.length ;i++)
        	System.out.print((char)(arr[i]));
        if (processComplete == 0) {
            status = true;
        } else {
            status = false;
        }
        return status;
    }

	private void initFileChooser() {
		String path = Home_Controller.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		String decodedPath = "";
		try {
			decodedPath = URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		fileChooser = new JFileChooser(new File(decodedPath));
		fileChooser.setFileFilter(new FileNameExtensionFilter("SQL Files", "sql"));
		fileChooser.setMultiSelectionEnabled(false);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
	}

	private void editProfile() {
		userControl.profile_Form.showUser_Form(user);
	}

	private void about() {
		About.getInstance().setVisible(true);
	}

	private void help() {
		Help.getInstance().setVisible(true);
	}

	private void confirmLogout() {
		if(JOptionPane.showConfirmDialog(homeFrame, "Are you sure you want to logout?", "Logout?", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
			logout();
	}

	private void logout() {
		homeFrame.setVisible(false);
		login.showLogin();
	}

	private void transferFocusToPassword() {
		login.usernameTF.transferFocus();
	}

	private void login() {
		try {
			User user = login.authenticate(sqlConnect);
			if(user == null){
				JOptionPane.showMessageDialog(homeFrame, "Username and password does not match!", "Authentication Error!", JOptionPane.ERROR_MESSAGE);
				return;
			}
			Home_Controller.user.define(user.getId(), user.getUsername(), user.getPassword());
			login.setVisible(false);
			homeFrame.showFrame(user);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Something went wrong when authenticating the user! The error message is: "+e.getMessage(), "Authentication Error!", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	class CloseApplicationListener extends WindowAdapter{
		@Override
		public void windowClosing(WindowEvent e) {
			if(JOptionPane.showConfirmDialog((Window)(e.getSource()), "Are you sure you want to exit?", "Confirm", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION){
				try {
					sqlConnect.closeConnection();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				System.exit(0);
			}
		}
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		
		if(homeFrame.tabbedPane.getSelectedIndex() == 0){
			facultyControl.updatePageNum(0);
			Faculty_List.getInstance().update_FacultyList();
			facultyControl.faculty_tab.update(Faculty_List.getInstance().facList);
		}
		
		if (homeFrame.tabbedPane.getSelectedIndex() == 1) {
			commControl.comm_tab.page = 0;
			commControl.search(0, "", 0);
			commControl.comm_tab.updateCBPage();
		}
		
		if(homeFrame.tabbedPane.getSelectedIndex() == 2){
			reportControl.report_Tab.options.setSelectedIndex(0);
			reportControl.report_Tab.search.setText("");
			reportControl.showAll();
		}
		
	}
}
