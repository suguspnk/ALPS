package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.DocumentException;

import model.faculty.Faculty;
import model.report.GeneratePDF;
import model.report.Report;
import model.report.Report_List;
import view.report.*;

public class Report_Controller implements ActionListener{
	
	public Report_Form reportForm;
	public Report_Profile reportProfile;
	public Report_Tab report_Tab;
	public Report_List reportList;
	
	private Object data[];
	private int selected;
	
	public Report_Controller(){
		reportForm = new Report_Form();
		reportProfile = new Report_Profile();
		report_Tab = new Report_Tab();
		
		reportForm.getAddButton().addActionListener(this);
		reportForm.btnCancel.addActionListener(this);
		
		report_Tab.addBtn.addActionListener(this);
		report_Tab.searchBtn.addActionListener(this);
		report_Tab.search.addActionListener(this);
		report_Tab.page.addActionListener(this);
		
		reportProfile.generatePDF.addActionListener(this);
		reportProfile.btnCancel.addActionListener(this);
		reportProfile.deleteReport.addActionListener(this);
		
		report_Tab.tbl.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				  int row = report_Tab.tbl.rowAtPoint(e.getPoint());
			      int col = report_Tab.tbl.columnAtPoint(e.getPoint());
			      if(col == 4 && row >= 0 && row<report_Tab.tbl.getRowCount()){
			    	  System.out.println("row: " + row + " col: " + col);
			    	  //profile.showProfile(row);
			    	  data = new Object[4];
			    	  data[0] = report_Tab.tbl.getValueAt(row, 0);
			    	  data[1] = report_Tab.tbl.getValueAt(row, 1);
			    	  data[2] = report_Tab.tbl.getValueAt(row, 2);
			    	  data[3] = report_Tab.tbl.getValueAt(row, 3);
			    	  selected = row + (report_Tab.page.getSelectedIndex() * 20);
			    	  generate(data);
			    	  reportProfile.setPreview(imgFolderPath+"/"+reportList.id[selected]+".pdf");
			    	  reportProfile.showDialog(true);
			      }
			  }
			});
	
	}
	
	public void showTab(){
		Home_Controller.homeFrame.tabbedPane.addTab("Report", report_Tab);
		
		updateTable();
	}
	
	private void updateTable(){
		reportList = new Report_List();
		Object[][] data = reportList.get(report_Tab.options.getSelectedIndex(), report_Tab.search.getText(), 0);
		updatePage();
		report_Tab.updateTable(data);
	}
	
	public void showAll(){
		reportList = new Report_List();
		Object[][] data = reportList.get(0, report_Tab.search.getText(), 0);
		updatePage();
		report_Tab.updateTable(data);
	}
	
	private void tablePage(int i){
		Object[][] data = reportList.get(report_Tab.options.getSelectedIndex(), report_Tab.search.getText(), report_Tab.page.getSelectedIndex());
		report_Tab.page.getSelectedIndex();
		report_Tab.updateTable(data);
	}
	
	private void updatePage(){
		int idlen = reportList.id.length;
		String[] strPage = new String[] {"1"}; 
		if( idlen <= 20){
			report_Tab.updatePage(strPage);
		}
		else{
			int plen = idlen/20 + 1;
			strPage = new String[plen];
			for(int i = 0 ; i < plen;  i++){
				strPage[i] = ""+(i+1);
			}
		}
		report_Tab.updatePage(strPage);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == reportForm.getAddButton()){
			verifyForm(e);
			reportForm.showPanel(false);
			updateTable();
		}
		else if(e.getSource() == reportForm.btnCancel){
			reportForm.close();
		}
		else if(e.getSource() == report_Tab.addBtn){
			try {
				reportForm.updateFaculty(Faculty.getFaculties());
			} catch (ClassNotFoundException a) {
				a.printStackTrace();
			} catch (SQLException a) {
				a.printStackTrace();
			}
			reportForm.showPanel(true);
		}
		else if(e.getSource() == report_Tab.search || e.getSource() == report_Tab.searchBtn){
			updateTable();	
		}
		else if(e.getSource() == report_Tab.page){
			tablePage(report_Tab.page.getSelectedIndex());
			System.out.println("tow: " + reportList.id.length);
		}
		else if(e.getSource() == reportProfile.generatePDF){
			//generate(data);
			reportProfile.showDialog(false);
		}
		else if(e.getSource() == reportProfile.btnCancel){
			reportProfile.showDialog(false);
			deleteFile();
		}
		else if(e.getSource() == reportProfile.deleteReport){
			new Report().delete(reportList.id[selected]);
			reportProfile.showDialog(false);
			deleteFile();
			updateTable();
			updatePage();
		}
	}
	
	String imgFolderPath = javax.swing.filechooser.FileSystemView.getFileSystemView().getDefaultDirectory().getAbsolutePath()+"/eFRMS/PDF";
	private void deleteFile(){
		File file = new File(imgFolderPath+"/"+reportList.id[selected]+".pdf");
		System.gc();
		file.delete();
	}
	
	public void generate(Object[] data){
		try {
			File file = new File(imgFolderPath);
			if(!file.exists() || !file.isDirectory())
				file.mkdirs();
			new GeneratePDF().createPdf(imgFolderPath+"/"+reportList.id[selected]+".pdf", data);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
	private void verifyForm(ActionEvent e){
		if(reportForm.verifyInput()){
			System.out.println("correct report form info");
			
			String fromdateString = reportForm.fmm.getSelectedIndex() + "-" + reportForm.fdd.getSelectedIndex() +"-"+ (reportForm.fyy.getSelectedIndex()+2000);
			String todateString = reportForm.tmm.getSelectedIndex() + "-" + reportForm.tdd.getSelectedIndex() +"-"+ (reportForm.tyy.getSelectedIndex()+2000);
			SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
			
			try {
				Date todate = format.parse(todateString);
				Date fromdate = format.parse(fromdateString);
				//System.out.println(fromdate);
				//System.out.println(todate);
				//*******
				if(todate.after(fromdate)){
					Date date = new Date();
					int type = 0;
					if(reportForm.generalRbtn.isSelected() == false)
						type = 1;
					
					java.sql.Date sqlDate = new java.sql.Date(date.getTime());
					java.sql.Date sqlfromDate = new java.sql.Date(fromdate.getTime());
					java.sql.Date sqltoDate = new java.sql.Date(todate.getTime());
					
					new Report( type, sqlDate, reportForm.faculty.getSelectedItem()+"", sqlfromDate, sqltoDate).save();
				}
				else 
					System.out.println("DATE IS NOT CORRECT FOK");
				//********
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		}
		else
			System.out.println("wrong report form info");

	}
	
	
}
