package main;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.Painter;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.ini4j.InvalidFileFormatException;

import model.AutoBackup;
import model.Config;
import view.additionalFunctionalities.Database_Password;
import controller.Home_Controller;

public class Main {
	
	public static Home_Controller homeControl;
	
	public static void main(String args[]) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException{
		config();
		Config.detectAdditionFrameSize();
		lookAndFeel();
		new Database_Password();
	}

	private static void lookAndFeel() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		UIManager.getLookAndFeelDefaults().put("Panel.background", Color.WHITE);
		UIManager.getLookAndFeelDefaults().put("OptionPane.background", Color.WHITE);
		UIManager.getLookAndFeelDefaults().put("FileChooser.background", Color.WHITE);
		UIManager.getLookAndFeelDefaults().put("FileChooser[Enabled].backgroundPainter",
		                    new Painter<JFileChooser>()
		                    {
		                        @Override
		                        public void paint(Graphics2D g, JFileChooser object, int width, int height)
		                        {
		                            g.setColor(Color.WHITE);
		                            g.draw(object.getBounds());

		                        }
		                    });
	}

	public static void start_Application(String dbPassword){
		Config.password = dbPassword;
		autoBackup();
		homeControl = new Home_Controller();
	}

	private static void autoBackup() {
		new AutoBackup();
	}

	private static void config() {
		try {
			Config.initConfig();
		} catch (InvalidFileFormatException e) {
			JOptionPane.showMessageDialog(null, "The ini file was an invalid file.", "Invalid ini file", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "An error occurred when creating the config file. The error message is: "+e.getMessage(), "Create config error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
}