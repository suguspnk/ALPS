package others;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

public class Util {
	
	private static Util util;
	
	public JFileChooser browser;
	public static File file;
	
	public Util(){
		browser = new  JFileChooser();
		SwingUtilities.updateComponentTreeUI(browser);
		browser.setFileFilter(new TypeOfFile());
		browser.setAcceptAllFileFilterUsed(false);
	}
	
	public static String toString(char[] sequence){
		String str = "";
		for(char c : sequence)
			str+=c;
		return str;
	}

	public static String insertBackSlash(String str) {
		String result = "";
		for(int i = 0; i < str.length(); result+=str.charAt(i++))
			if(!Character.isLetterOrDigit(str.charAt(i)))	// if the character is not a letter or a number
				result+="\\";
		return result;
	}
	
	public static BufferedImage resizeImage(BufferedImage origImg, int IMG_WIDTH, int IMG_HEIGHT){
        if(origImg == null)
            return null;
        try{
            int type = origImg.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : origImg.getType();
            BufferedImage resizedImg = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
            Graphics2D g = resizedImg.createGraphics();
            g = render(g);
            g.drawImage(origImg, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
            g.setComposite(AlphaComposite.Src);
            g.dispose();
            return resizedImg;
        }catch(Exception e){
            System.err.println("resizeImage method error: "+e.getLocalizedMessage());
        }
        return origImg;
	}
	
	public static Graphics2D render(Graphics2D g){
		g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION,RenderingHints.VALUE_ALPHA_INTERPOLATION_DEFAULT);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_DEFAULT);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_DEFAULT);
		g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_DEFAULT);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_DEFAULT);
		g.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DEFAULT);
		return g;
	}
	 
	public String chooseImage() throws IOException{
		
		int valFile = browser.showOpenDialog(null);
		if(valFile == JFileChooser.APPROVE_OPTION){
			file = browser.getSelectedFile();
		}
		else
			return "";
		
		return file.getAbsolutePath();
	}
	
	public String moveImage(File file,String lName,String fName,String mName){
		String path = "";
		String imgFolderPath = javax.swing.filechooser.FileSystemView.getFileSystemView().getDefaultDirectory().getAbsolutePath()+"/eFRMS/faculty_photos";
		File imgFolder = new File(imgFolderPath);
		if(!imgFolder.exists() || !imgFolder.isDirectory())
			imgFolder.mkdirs();
		InputStream inStream = null;
		OutputStream outStream = null;
	 
	    	try{
	
	    	    File bfile =new File(imgFolderPath+"/" + lName + "_" + fName + "_" + mName + ".jpg" );
	 
	    	    inStream = new FileInputStream(file);
	    	    outStream = new FileOutputStream(bfile);
	 
	    	    byte[] buffer = new byte[1024];
	 
	    	    int length;
	    	    //copy the file content in bytes 
	    	    while ((length = inStream.read(buffer)) > 0){
	    	    	outStream.write(buffer, 0, length);
	    	    }
	 
	    	    inStream.close();
	    	    outStream.close();
	    	    
	    	    path = bfile.getAbsolutePath();
	    	    System.out.println("File is copied successfully!");
	 
	    	}catch(IOException e){
	    	    e.printStackTrace();
	    	}
	    	
	    	return path;
	}
	
	
	public static Util getInstance(){
		
		if(util == null)
			util = new Util();
		return util;
	}
	
	class TypeOfFile extends FileFilter  {
		
		public boolean accept(File file){
			return file.isDirectory() || file.getName().toLowerCase().endsWith(".png") || file.getName().toLowerCase().endsWith(".jpeg") || file.getName().toLowerCase().endsWith(".jpg");
		}
	  
	 	//Set description for the type of file that should be display  
	 	public String getDescription() {  
	 		return "Image files";  
	 	}
	}
	
	public static class TableCheckBoxListener extends MouseAdapter{

		private JTable table;
		private JCheckBox checkboxall;
		public int counter;
		private int checkboxIndex;

		public TableCheckBoxListener(JTable table, JCheckBox checkboxall, int checkboxIndex) {
			this.table = table;
			this.checkboxall = checkboxall;
			this.checkboxIndex = checkboxIndex;
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			if(e.getSource() == table){
				int row = e.getY()/table.getRowHeight();
				if(row >= table.getRowCount())
					return;
				boolean isChecked = !(Boolean)table.getValueAt(row, checkboxIndex);
				setCheckedRows(isChecked ? ++counter : --counter);
				table.setValueAt(isChecked, row, 0);
			}
			else if(e.getSource() == checkboxall){
				if(checkboxall.isSelected())
					selectAll();
				else
					deselectAll();
			}
		}

		private void selectAll() {
			counter = table.getRowCount();
			setSelectedActivities(true);
		}

		private void deselectAll() {
			counter = 0;
			setSelectedActivities(false);
		}
		
		public void setCheckedRows(int checkedRows) {
			counter = checkedRows;
			checkboxall.setSelected(checkedRows == table.getRowCount());
		}
		
		public void setSelectedActivities(boolean b) {
			for(int i = 0; i < table.getRowCount(); i++)
				table.setValueAt(b, i, 0);
		}
	}
}